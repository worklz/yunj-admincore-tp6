<?php
// +----------------------------------------------------------------------
// | 云静Admin
// +----------------------------------------------------------------------
// | Copyright (c) 2019-2022 http://www.iyunj.cn
// +----------------------------------------------------------------------
// | 云静Admin提供个人非商业用途免费使用。
// +----------------------------------------------------------------------
// | Author: Uncle-L <1071446619@qq.com>
// +----------------------------------------------------------------------
// | 请求配置
// +----------------------------------------------------------------------

use yunj\app\demo\enum\State;
use yunj\core\constants\BuilderConst;
use yunj\app\admin\enum\auth\AuthType;
use yunj\app\admin\enum\EnableLoginAuth;
use yunj\app\admin\enum\TableBuilderEvent;
use yunj\app\admin\enum\route\RequestMethod;
use yunj\app\demo\controller\IndexController;
use yunj\app\demo\controller\controller\IndexController as ControllerIndexController;
use yunj\app\demo\controller\view\IndexController as ViewIndexController;
use yunj\app\demo\controller\view\BlockRewriteController as ViewBlockRewriteController;
use yunj\app\demo\controller\validate\ThinkphpController as ValidateThinkphpController;
use yunj\app\demo\controller\validate\FrontController as ValidateFrontController;
use yunj\app\demo\controller\form\FieldValidateController as FormFieldValidateController;
use yunj\app\demo\controller\form\IndexController as FormIndexController;
use yunj\app\demo\controller\form\SingleController as FormSingleController;
use yunj\app\demo\controller\table\IndexController as TableIndexController;
use yunj\app\demo\controller\import\IndexController as ImportIndexController;
use yunj\app\demo\controller\user\AllController as UserAllController;
use yunj\app\demo\controller\user\IndexController as UserIndexController;

// 处理构建器路由请求项（分别增加为顶部和侧边栏的形式）
function __handle_builder_requests(array $items) {
    $topItems = [];
    $sidebarItems = [];
    /** @var AuthType $topAuthTypeObj */
    $topAuthTypeObj = AuthType::TOP_MENU();
    /** @var AuthType $sidebarAuthTypeObj */
    $sidebarAuthTypeObj = AuthType::SIDEBAR_MENU();
    foreach ($items as $item) {
        $item['desc'] = $item['desc'] ?? '';
        $item['require_params'] = $item['require_params'] ?? [];
        $topItem = $sidebarItem = $item;
        $topItem['system_key'] = md5($item['system_key'] . '_top');
        $sidebarItem['system_key'] = md5($item['system_key'] . '_sidebar');
        $topItem['desc'] .= ($item['desc'] ? "(" : "") . $topAuthTypeObj->getTitle() . ($item['desc'] ? ")" : "");
        $sidebarItem['desc'] .= ($item['desc'] ? "(" : "") . $sidebarAuthTypeObj->getTitle() . ($item['desc'] ? ")" : "");
        $topItem['require_params']['originLocation'] = $topAuthTypeObj->getValue();
        $sidebarItem['require_params']['originLocation'] = $sidebarAuthTypeObj->getValue();
        $topItems[] = $topItem;
        $sidebarItems[] = $sidebarItem;
    }
    return array_merge($topItems, $sidebarItems);
}

// 处理demo标识
function __handle_demo_make(array $requests): array {
    foreach ($requests as $k => $v) { // 此处不能使用&引用的方式，会导致流转到后面的文件调用，引发异常
        $v['is_demo'] = \yunj\app\admin\enum\IsDemo::YES;
        if ($v['items'] ?? []) {
            $v['items'] = __handle_demo_make($v['items']);
        }
        $requests[$k] = $v;
    }
    return $requests;
}

/** @var AuthType $topAuthTypeObj */
$topAuthTypeObj = AuthType::TOP_MENU();
/** @var AuthType $sidebarAuthTypeObj */
$sidebarAuthTypeObj = AuthType::SIDEBAR_MENU();

$requests = [
    [
        'system_key' => '73103a93607edd0dafd106694d86a312',
        'type' => 'group',
        'name' => YUNJ_ADMIN_ENTRANCE . '/yunj/demo',
        'namespace' => '',
        'desc' => '后台云静DEMO',
        'enable_login_auth' => EnableLoginAuth::ON,
        'items' => [
            [
                'system_key' => '40a77da238a7c556a80a6aea079b5359',
                'type' => 'route',
                'name' => 'yunjDemoWelcome',
                'rule' => 'welcome',
                'route' => class_route([IndexController::class, 'welcome']),
                'desc' => 'Welcome',
                'items' => [
                    [
                        'system_key' => '961e7e61a81f6191d3f0714232285410',
                        'type' => 'request',
                        'desc' => 'Welcome',
                    ]
                ]
            ],
            [
                'system_key' => '585b71dccb99bfc2f2fea1aac0abe740',
                'type' => 'route',
                'name' => 'yunjDemoControllerInstruction',
                'rule' => 'controller/instruction',
                'route' => class_route([ControllerIndexController::class, 'instruction']),
                'desc' => '控制器使用说明',
                'items' => [
                    [
                        'system_key' => '1cf758a217f89c5fc042b5054afc6ca1',
                        'type' => 'request',
                        'desc' => '控制器使用说明',
                    ]
                ]
            ],
            [
                'system_key' => '0a5f01c30fb1145c1bdaefaf94cb943e',
                'type' => 'group',
                'name' => 'view',
                'namespace' => '',
                'desc' => '视图',
                'items' => [
                    [
                        'system_key' => '1b2f5c6bfb2edee4e1c7b848ffd72d5e',
                        'type' => 'route',
                        'name' => 'yunjDemoViewInstruction',
                        'rule' => 'instruction',
                        'route' => class_route([ViewIndexController::class, 'instruction']),
                        'desc' => '使用说明',
                        'items' => [
                            [
                                'system_key' => '0b4454cb148cf28d91660d471c5b47de',
                                'type' => 'request',
                                'desc' => '使用说明',
                            ]
                        ]
                    ],
                    [
                        'system_key' => '66bcf0f050f5182df6284aa610c97ed6',
                        'type' => 'route',
                        'name' => 'yunjDemoViewIcon',
                        'rule' => 'icon',
                        'route' => class_route([ViewIndexController::class, 'icon']),
                        'desc' => '图标',
                        'items' => [
                            [
                                'system_key' => '22edf0d3498a66cc173234c0dc8894ad',
                                'type' => 'request',
                                'desc' => '图标',
                            ]
                        ]
                    ],
                    [
                        'system_key' => 'cbed3a65480ad2a5a0f5d02e94c81dfd',
                        'type' => 'group',
                        'name' => 'block',
                        'namespace' => '',
                        'desc' => '视图区块',
                        'items' => [
                            [
                                'system_key' => 'afcfbe3381d8007d4f512071e4a82647',
                                'type' => 'route',
                                'name' => 'yunjDemoViewBlockSeo',
                                'rule' => 'seo',
                                'route' => class_route([ViewBlockRewriteController::class, 'seo']),
                                'desc' => 'seo',
                                'items' => [
                                    [
                                        'system_key' => 'a692796ae9817d83454080fd75e36ae9',
                                        'type' => 'request',
                                        'desc' => 'seo',
                                    ]
                                ]
                            ],
                            [
                                'system_key' => '95deb2cd417346f3950df23d48bf8ddc',
                                'type' => 'route',
                                'name' => 'yunjDemoViewBlockHeadStyle',
                                'rule' => 'head-style',
                                'route' => class_route([ViewBlockRewriteController::class, 'headStyle']),
                                'desc' => 'headStyle',
                                'items' => [
                                    [
                                        'system_key' => '7d5788ebedf9c6e147b5f356ca91f723',
                                        'type' => 'request',
                                        'desc' => 'headStyle',
                                    ]
                                ]
                            ],
                            [
                                'system_key' => '91cefb7f3bdb9158f382a02ca0c49b99',
                                'type' => 'route',
                                'name' => 'yunjDemoViewBlockHeadScript',
                                'rule' => 'head-script',
                                'route' => class_route([ViewBlockRewriteController::class, 'headScript']),
                                'desc' => 'headScript',
                                'items' => [
                                    [
                                        'system_key' => 'fe992e3786ecdb71a861a28e47430908',
                                        'type' => 'request',
                                        'desc' => 'headScript',
                                    ]
                                ]
                            ],
                            [
                                'system_key' => 'dd244469ece0f87dc1c3b565cfed03c2',
                                'type' => 'route',
                                'name' => 'yunjDemoViewBlockContent',
                                'rule' => 'content',
                                'route' => class_route([ViewBlockRewriteController::class, 'content']),
                                'desc' => 'content',
                                'items' => [
                                    [
                                        'system_key' => 'c0c5d22e8a6942476f2f6fd5e0160d2a',
                                        'type' => 'request',
                                        'desc' => 'content',
                                    ]
                                ]
                            ],
                            [
                                'system_key' => 'ebd59d709e951004a39fbc7163bcd863',
                                'type' => 'route',
                                'name' => 'yunjDemoViewBlockScript',
                                'rule' => 'script',
                                'route' => class_route([ViewBlockRewriteController::class, 'script']),
                                'desc' => 'script',
                                'items' => [
                                    [
                                        'system_key' => '78265f147832621c3970f504ddaf203a',
                                        'type' => 'request',
                                        'desc' => 'script',
                                    ]
                                ]
                            ],
                        ]
                    ]
                ]
            ],
            [
                'system_key' => 'fe601e432f60e32ea0d3dc544067d7c8',
                'type' => 'group',
                'name' => 'validate',
                'namespace' => '',
                'desc' => '验证器',
                'items' => [
                    [
                        'system_key' => 'f48a8a669213cca62d51afd2e993e59a',
                        'type' => 'route',
                        'name' => 'yunjDemoValidateTp',
                        'rule' => 'tp',
                        'route' => class_route([ValidateThinkphpController::class, 'index']),
                        'desc' => 'TP验证器',
                        'items' => [
                            [
                                'system_key' => 'e3018dc6d6a170d18a1bbfc77f538274',
                                'type' => 'request',
                                'desc' => '页面',
                                'method' => RequestMethod::GET,
                            ],
                            [
                                'system_key' => '9b3fda24c6b5adebdb87d90e4ef65144',
                                'type' => 'request',
                                'desc' => '验证数据',
                                'method' => RequestMethod::POST,
                            ],
                        ]
                    ],
                    [
                        'system_key' => 'fae0d047a1f5f3842c86f4f23a8ca8a4',
                        'type' => 'route',
                        'name' => 'yunjDemoValidateFront',
                        'rule' => 'front',
                        'route' => class_route([ValidateFrontController::class, 'index']),
                        
                        'desc' => '前端验证器',
                        'items' => [
                            [
                                'system_key' => 'fa2172b33f43065052ad0192547aea0d',
                                'type' => 'request',
                                'desc' => '前端验证器',
                            ]
                        ]
                    ],
                ]
            ],
            [
                'system_key' => 'f16483ecf69dfd3bc3c8cbb6778cb814',
                'type' => 'group',
                'name' => 'form-builder',
                'namespace' => '',
                'desc' => '表单构建器',
                'items' => [
                    [
                        'system_key' => '5612e247fe78e499137fe3b0a53e0a17',
                        'type' => 'route',
                        'name' => 'yunjDemoFormBuilderSample',
                        'rule' => 'sample',
                        'route' => class_route([FormIndexController::class, 'sample']),
                        
                        'desc' => '简单示例',
                        'items' => __handle_builder_requests([
                            [
                                'system_key' => '4d09a3381c2ffc146ef69fc618117f17',
                                'type' => 'request',
                                'desc' => '页面',
                                'method' => RequestMethod::GET,
                            ],
                            [
                                'system_key' => '9afbf6d6b37fa6bad0e288a95ba183cd',
                                'type' => 'request',
                                'desc' => '数据提交',
                                'method' => RequestMethod::POST,
                            ],
                        ]),
                    ],
                    [
                        'system_key' => 'f6cfbfc2dfcca39f4cc83e4eddcad257',
                        'type' => 'route',
                        'name' => 'yunjDemoFormBuilderChain',
                        'rule' => 'chain',
                        'route' => class_route([FormIndexController::class, 'chain']),
                        
                        'desc' => '链式操作',
                        'items' => __handle_builder_requests([
                            [
                                'system_key' => 'f7f14132ce2ad5a5cf5204e0a5ef3143',
                                'type' => 'request',
                                'desc' => 'Demo 表单构建器链式操作页面',
                                'method' => RequestMethod::GET,
                            ],
                            [
                                'system_key' => '232552d7ddec27f528a0ff21425f65a7',
                                'type' => 'request',
                                'desc' => '数据提交',
                                'method' => RequestMethod::POST,
                            ],
                        ]),
                    ],
                    [
                        'system_key' => '1a25fc80caeb835622c855cdac34d4e2',
                        'type' => 'route',
                        'name' => 'yunjDemoFormBuilderArrayConfig',
                        'rule' => 'array-config',
                        'route' => class_route([FormIndexController::class, 'arrayConfig']),
                        
                        'desc' => '数组配置',
                        'items' => __handle_builder_requests([
                            [
                                'system_key' => '4b7ffeb4bcdf6bef80bb8f61c5699492',
                                'type' => 'request',
                                'desc' => '页面',
                                'method' => RequestMethod::GET,
                            ],
                            [
                                'system_key' => '077993ad2ac5a9a5c48f813b9f5761fb',
                                'type' => 'request',
                                'desc' => '数据提交',
                                'method' => RequestMethod::POST,
                            ],
                            [
                                'system_key' => 'd1df74ce745269a4047898d9ad67e0fb',
                                'type' => 'request',
                                'desc' => '弹窗页面',
                                'method' => RequestMethod::GET,
                                'require_params' => [
                                    'isPopup' => 'yes'
                                ],
                            ],
                            [
                                'system_key' => 'fa439a773b0b576b01501452c7dad3ca',
                                'type' => 'request',
                                'desc' => '弹窗数据提交',
                                'method' => RequestMethod::POST,
                                'require_params' => [
                                    'isPopup' => 'yes'
                                ],
                            ],
                        ]),
                    ],
                    [
                        'system_key' => 'b9f2df6eb14729a7ec8ce74dc003ab18',
                        'type' => 'route',
                        'name' => 'yunjDemoFormBuilderFieldValidate',
                        'rule' => 'field-validate',
                        'route' => class_route([FormFieldValidateController::class, 'index']),
                        
                        'desc' => '字段验证',
                        'items' => __handle_builder_requests([
                            [
                                'system_key' => '69bb628fc135fd6435e006701ec9e1a2',
                                'type' => 'request',
                                'desc' => 'Demo 表单构建器字段验证器页面',
                                'method' => RequestMethod::GET,
                            ],
                            [
                                'system_key' => '8c369dd5eeffcfa36c94898edd973a7d',
                                'type' => 'request',
                                'desc' => 'Demo 表单构建器字段验证器数据提交处理',
                                'method' => RequestMethod::POST,
                            ],
                        ]),
                    ],
                    [
                        'system_key' => '2df724f25bf63334f76f58ac85a307a2',
                        'type' => 'route',
                        'name' => 'yunjDemoFormBuilderCodePreview',
                        'rule' => 'field-validate-code-preview',
                        'route' => class_route([FormFieldValidateController::class, 'codePreview']),
                        
                        'desc' => '字段验证代码预览',
                        'items' => __handle_builder_requests([
                            [
                                'system_key' => '4cd24e43c4efcf72cff3796de3bdf245',
                                'type' => 'request',
                                'desc' => '字段验证代码预览',
                            ]
                        ]),
                    ],
                    [
                        'system_key' => '90ac6de597d302850e960fa384295ada',
                        'type' => 'route',
                        'name' => 'yunjDemoFormBuilderSingle',
                        'rule' => 'single',
                        'route' => class_route([FormSingleController::class, 'index']),
                        
                        'desc' => '单一字段调用',
                        'items' => __handle_builder_requests([
                            [
                                'system_key' => '3faf0faacf74de8dd6bfbe6d5fb17089',
                                'type' => 'request',
                                'desc' => '单一字段调用',
                            ]
                        ]),
                    ],
                    [
                        'system_key' => 'dd50145eb186222f0670f1e5179b57e8',
                        'type' => 'route',
                        'name' => 'yunjDemoFormBuilderDropdownSearchOptions',
                        'rule' => 'form-dropdown-search-options',
                        'route' => class_route([FormIndexController::class, 'dropdownSearchOptions']),
                        
                        'desc' => '下拉搜索',
                        'items' => __handle_builder_requests([
                            [
                                'system_key' => '1079a371c5465499726a309c7fd20ccf',
                                'type' => 'request',
                                'desc' => '下拉搜索',
                            ]
                        ]),
                    ]
                ]
            ],
            [
                'system_key' => '643152fa700d937a9b5856021721021f',
                'type' => 'group',
                'name' => 'table-builder',
                'namespace' => '',
                'desc' => '表格构建器',
                'items' => [
                    [
                        'system_key' => 'd910213d51203554457d36d4af17de16',
                        'type' => 'route',
                        'name' => 'yunjDemoTableBuilderSample',
                        'rule' => 'sample',
                        'route' => class_route([TableIndexController::class, 'sample']),
                        
                        'desc' => '简单示例',
                        'items' => __handle_builder_requests([
                            [
                                'system_key' => '78600c358764de822918dc887a3187ee',
                                'type' => 'request',
                                'desc' => '简单示例'
                            ]
                        ]),
                    ],
                    [
                        'system_key' => 'd9651dcb814ca172946f7ff5e6b237ad',
                        'type' => 'route',
                        'name' => 'yunjDemoTableBuilderChain',
                        'rule' => 'chain',
                        'route' => class_route([TableIndexController::class, 'chain']),
                        
                        'desc' => '链式操作',
                        'items' => __handle_builder_requests([
                            [
                                'system_key' => '4ec655268a61a1641e25f7aa745bbf7a',
                                'type' => 'request',
                                'desc' => '链式操作'
                            ]
                        ]),
                    ],
                    [
                        'system_key' => 'c6342366a3a77e90af637f809da63939',
                        'type' => 'route',
                        'name' => 'yunjDemoTableBuilderArrayConfig',
                        'rule' => 'array-config',
                        'route' => class_route([TableIndexController::class, 'arrayConfig']),
                        
                        'desc' => '数组配置',
                        'items' => __handle_builder_requests([
                            [
                                'system_key' => '49644ec6dddf4dc071224f8ff9a30262',
                                'type' => 'request',
                                'desc' => 'Demo 表格构建器数组配置',
                            ],
                            [
                                'system_key' => '4d54e02c0192b328a5165117c87bb478',
                                'type' => 'request',
                                'desc' => 'Demo 表格构建器数组配置(弹窗)',
                                'require_params' => [
                                    'isPopup' => 'yes',
                                ],
                            ]
                        ]),
                    ],
                    [
                        'system_key' => 'fad009672e6326e76a00568402db9376',
                        'type' => 'route',
                        'name' => 'yunjDemoTableBuilderTree',
                        'rule' => 'tree',
                        'route' => class_route([TableIndexController::class, 'tree']),
                        
                        'desc' => '树形表格',
                        'items' => __handle_builder_requests([
                            [
                                'system_key' => 'a2605edf12f51c468f9218a88e5b3bdc',
                                'type' => 'request',
                                'desc' => '树形表格'
                            ]
                        ]),
                    ],
                    [
                        'system_key' => 'fb4b5e09c3c850ef49213a331804844d',
                        'type' => 'route',
                        'name' => 'yunjDemoTableBuilderDragSort',
                        'rule' => 'drag-sort',
                        'route' => class_route([TableIndexController::class, 'dragSort']),
                        
                        'desc' => '拖拽排序',
                        'items' => __handle_builder_requests([
                            [
                                'system_key' => '1360949c50c6cc02711ed850186010c8',
                                'type' => 'request',
                                'desc' => '拖拽排序'
                            ],
                            [
                                'system_key' => '5ce728722b3a1626396d376f1766582f',
                                'type' => 'request',
                                'desc' => '数据排序',
                                'method' => RequestMethod::POST,
                                'require_params' => [
                                    BuilderConst::ASYNC_TYPE_KEY => 'event',
                                    BuilderConst::ASYNC_EVENT_KEY => 'sort',
                                ],
                            ],
                        ]),
                    ],
                    [
                        'system_key' => 'fd657c3411fc28d606bdf78d0a810ba2',
                        'type' => 'route',
                        'name' => 'yunjDemoTableBuilderPk',
                        'rule' => 'pk',
                        'route' => class_route([TableIndexController::class, 'pk']),
                        
                        'desc' => '自定义主键',
                        'items' => __handle_builder_requests([
                            [
                                'system_key' => '00ab39c7bb7990154ce26af93ae0c4f1',
                                'type' => 'request',
                                'desc' => '自定义主键'
                            ],
                            [
                                'system_key' => '7ce5d2ba0af5351779d0c39ff286da4d',
                                'type' => 'request',
                                'desc' => '后端获取数据主键',
                                'method' => RequestMethod::POST,
                                'require_params' => [
                                    BuilderConst::ASYNC_TYPE_KEY => 'event',
                                    BuilderConst::ASYNC_EVENT_KEY => 'back_get_pk',
                                ],
                            ],
                        ]),
                    ],
                    [
                        'system_key' => '132d886ded93ff51fd044a87614d2ed7',
                        'type' => 'route',
                        'name' => 'yunjDemoTableBuilderPkCodeView',
                        'rule' => 'pk-code-view',
                        'route' => class_route([TableIndexController::class, 'pkCodeView']),
                        
                        'desc' => '自定义主键代码预览',
                        'items' => __handle_builder_requests([
                            [
                                'system_key' => 'b69677c5ee4ec1f8bbc07d71d31708d9',
                                'type' => 'request',
                                'desc' => '自定义主键代码预览',
                            ]
                        ]),
                    ]
                ]
            ],
            [
                'system_key' => '3173415bbfc3f7a17b2e71a00741a703',
                'type' => 'group',
                'name' => 'import-builder',
                'namespace' => '',
                'desc' => '导入构建器',
                'items' => [
                    [
                        'system_key' => '9e51a856edde30070e927c449d46f14f',
                        'type' => 'route',
                        'name' => 'yunjDemoImportBuilderSample',
                        'rule' => 'sample',
                        'route' => class_route([ImportIndexController::class, 'sample']),
                        
                        'desc' => '简单示例',
                        'items' => __handle_builder_requests([
                            [
                                'system_key' => '8eea1848db3db82201e734138ea771b2',
                                'type' => 'request',
                                'desc' => '页面',
                                'method' => RequestMethod::GET,
                            ],
                            [
                                'system_key' => '4d42cd725db0a01347ccf632a8d2ee5c',
                                'type' => 'request',
                                'desc' => '数据导入处理',
                                'method' => RequestMethod::POST,
                            ],
                        ]),
                    ],
                    [
                        'system_key' => '759ef38e6c8289411fd2232388cde02d',
                        'type' => 'route',
                        'name' => 'yunjDemoImportBuilderChain',
                        'rule' => 'chain',
                        'route' => class_route([ImportIndexController::class, 'chain']),
                        
                        'desc' => '链式操作',
                        'items' => __handle_builder_requests([
                            [
                                'system_key' => '68981b7d79a2884793b7b0d1f618983c',
                                'type' => 'request',
                                'desc' => '页面',
                                'method' => RequestMethod::GET,
                            ],
                            [
                                'system_key' => '62d5c476a21c0b2f13df000b42891dc9',
                                'type' => 'request',
                                'desc' => '数据导入处理',
                                'method' => RequestMethod::POST,
                            ],
                        ]),
                    ],
                    [
                        'system_key' => 'df4a2a3d2ea65f4f1311e4ce5c74c42f',
                        'type' => 'route',
                        'name' => 'yunjDemoImportBuilderArrayConfig',
                        'rule' => 'array-config',
                        'route' => class_route([ImportIndexController::class, 'arrayConfig']),
                        
                        'desc' => '数组配置',
                        'items' => __handle_builder_requests([
                            [
                                'system_key' => 'fa685e71d6fbd95566b3d20974b678c6',
                                'type' => 'request',
                                'desc' => '页面',
                                'method' => RequestMethod::GET,
                            ],
                            [
                                'system_key' => 'eb7f2d20a30d37e6d1453e66fdd37e3c',
                                'type' => 'request',
                                'desc' => '数据导入处理',
                                'method' => RequestMethod::POST,
                            ],
                            [
                                'system_key' => '8e2adc591a480fed6f27820bf364b3e7',
                                'type' => 'request',
                                'desc' => '弹窗页面',
                                'method' => RequestMethod::GET,
                                'require_params' => [
                                    'isPopup' => 'yes'
                                ],
                            ],
                            [
                                'system_key' => '17d12cc4ae12d85daee49548bdce976d',
                                'type' => 'request',
                                'desc' => '弹窗数据导入处理',
                                'method' => RequestMethod::POST,
                                'require_params' => [
                                    'isPopup' => 'yes'
                                ],
                            ],
                        ]),
                    ],
                ]
            ],
            [
                'system_key' => 'd3d91d74842b67ce49df237bec5b5caf',
                'type' => 'group',
                'name' => 'user',
                'namespace' => '',
                'desc' => '用户管理',
                'items' => [
                    [
                        'system_key' => 'f84b3aaed16b254e4b55163628f12809',
                        'type' => 'route',
                        'name' => 'yunjDemoUserList',
                        'rule' => 'list',
                        'route' => class_route([UserIndexController::class, 'lists']),
                        
                        'desc' => '列表',
                        'items' => [
                            [
                                'system_key' => '88d0d40b31a4abe7c863649385282c31',
                                'type' => 'request',
                                'desc' => '页面',
                                'method' => RequestMethod::GET,
                            ],
                            [
                                'system_key' => 'c061f6a52842e58e65de22e677853a5b',
                                'type' => 'request',
                                'desc' => '数据获取',
                                'method' => RequestMethod::POST,
                            ],
                            [
                                'system_key' => '3287187e7dbcef37842f3ca483bc367a',
                                'type' => 'request',
                                'desc' => '数据导出',
                                'method' => RequestMethod::POST,
                                'require_params' => [BuilderConst::ASYNC_TYPE_KEY => 'export'],
                            ],
                            [
                                'system_key' => '247d050b7e39570ffebd99c50e314c03',
                                'type' => 'request',
                                'desc' => '数据移入回收站',
                                'method' => RequestMethod::POST,
                                'require_params' => [
                                    BuilderConst::ASYNC_TYPE_KEY => 'event',
                                    BuilderConst::ASYNC_EVENT_KEY => State::RECYLE_BIN,
                                ],
                            ],
                            [
                                'system_key' => 'fb4c42de52d494d705f81873d9c81dff',
                                'type' => 'request',
                                'desc' => '数据恢复正常',
                                'method' => RequestMethod::POST,
                                'require_params' => [
                                    BuilderConst::ASYNC_TYPE_KEY => 'event',
                                    BuilderConst::ASYNC_EVENT_KEY => State::NORMAL,
                                ],
                            ],
                            [
                                'system_key' => '4389d8dab9f7dd60db9eaf12341e1165',
                                'type' => 'request',
                                'desc' => '数据删除',
                                'method' => RequestMethod::POST,
                                'require_params' => [
                                    BuilderConst::ASYNC_TYPE_KEY => 'event',
                                    BuilderConst::ASYNC_EVENT_KEY => State::DELETED,
                                ],
                            ],
                        ]
                    ],
                    [
                        'system_key' => '6ae77299e685099268e160a81c94999e',
                        'type' => 'route',
                        'name' => 'yunjDemoUserAdd',
                        'rule' => 'add',
                        'route' => class_route([UserIndexController::class, 'add']),
                        
                        'desc' => '添加',
                        'items' => [
                            [
                                'system_key' => '4dac35c78392ab50cb38718cfd47662b',
                                'type' => 'request',
                                'desc' => "页面",
                                'method' => RequestMethod::GET,
                            ],
                            [
                                'system_key' => 'c8cf471e0ff111e65c6557f9801729c4',
                                'type' => 'request',
                                'desc' => '数据保存',
                                'method' => RequestMethod::POST,
                            ],
                            [
                                'system_key' => '08de65d6e22d0321cefa0fce9ab79f12',
                                'type' => 'request',
                                'desc' => '页面(弹窗打开)（顶部菜单）',
                                'method' => RequestMethod::GET,
                                'require_params' => [
                                    'isPopup' => 'yes',
                                    'originLocation' => $topAuthTypeObj->getValue()
                                ],
                            ],
                            [
                                'system_key' => 'dbb770ec73b7c720abf743407b7ef775',
                                'type' => 'request',
                                'desc' => '数据保存(弹窗打开)（顶部菜单）',
                                'method' => RequestMethod::POST,
                                'require_params' => [
                                    'isPopup' => 'yes',
                                    'originLocation' => $topAuthTypeObj->getValue()
                                ],
                            ],
                            [
                                'system_key' => '1c0478618e3718226f0c3bb357a7348e',
                                'type' => 'request',
                                'desc' => '页面(子页面)（顶部菜单）',
                                'method' => RequestMethod::GET,
                                'require_params' => [
                                    'originLocation' => $topAuthTypeObj->getValue()
                                ],
                            ],
                            [
                                'system_key' => 'e30289e901e61a243bc075829152b78a',
                                'type' => 'request',
                                'desc' => '数据保存(子页面)（顶部菜单）',
                                'method' => RequestMethod::POST,
                                'require_params' => [
                                    'originLocation' => $topAuthTypeObj->getValue()
                                ],
                            ],
                        ]
                    ],
                    [
                        'system_key' => '8b5a28bdee1340e7dc86c42a836cc1a1',
                        'type' => 'route',
                        'name' => 'yunjDemoUserEdit',
                        'rule' => 'edit',
                        'route' => class_route([UserIndexController::class, 'edit']),
                        
                        'desc' => '编辑',
                        'items' => [
                            [
                                'system_key' => '2ecdc3de49cf0093e65bf82d91a6878e',
                                'type' => 'request',
                                'desc' => '页面',
                                'method' => RequestMethod::GET,
                            ],
                            [
                                'system_key' => 'd2d6216fb5674ea40364f09541b534a0',
                                'type' => 'request',
                                'desc' => '数据保存',
                                'method' => RequestMethod::POST,
                            ],
                        ],
                    ],
                    [
                        'system_key' => '35115e7854d63390b4e5b9c1d95f7eb6',
                        'type' => 'route',
                        'name' => 'yunjDemoUserImport',
                        'rule' => 'import',
                        'route' => class_route([UserIndexController::class, 'import']),
                        
                        'desc' => '导入',
                        'items' => [
                            [
                                'system_key' => 'b5cd955c10cf71e56b28918a5c5242d1',
                                'type' => 'request',
                                'desc' => '页面',
                                'method' => RequestMethod::GET,
                            ],
                            [
                                'system_key' => '01db42cb9fd6173173a995d2273f33b3',
                                'type' => 'request',
                                'desc' => '数据处理',
                                'method' => RequestMethod::POST,
                            ],
                        ],
                    ],
                    [
                        'system_key' => '87a1483034d079c11baef0a784c48cfd',
                        'type' => 'route',
                        'name' => 'yunjDemoUserDropdownSearchOptions',
                        'rule' => 'user-dropdown-search-options',
                        'route' => class_route([UserIndexController::class, 'userDropdownSearchOptions']),
                        
                        'desc' => '下拉搜索',
                        'items' => [
                            [
                                'system_key' => 'a820a7b71a09cf01a6d304f9ed194c72',
                                'type' => 'request',
                                'desc' => 'Demo 用户下拉搜索',
                            ]
                        ],
                    ],
                ]
            ],
            [
                'system_key' => '3c9083d5561ba7c2036f3a94fb828497',
                'type' => 'group',
                'name' => 'all-user',
                'namespace' => '',
                'desc' => '所有用户',
                'items' => [
                    [
                        'system_key' => '802fa511cf4cd3dc59dd84ec3d3bd23d',
                        'type' => 'route',
                        'name' => 'yunjDemoAllUserList',
                        'rule' => 'list',
                        'route' => class_route([UserAllController::class, 'lists']),
                        
                        'desc' => '列表',
                        'items' => [
                            [
                                'system_key' => '5feaa4108777339fce385412fd0496c2',
                                'type' => 'request',
                                'desc' => '页面',
                                'method' => RequestMethod::GET,
                            ],
                            [
                                'system_key' => '4140735ca62f6f74db44af7cc9645598',
                                'type' => 'request',
                                'desc' => '数据获取',
                                'method' => RequestMethod::POST,
                            ],
                            [
                                'system_key' => 'a45b47de7f0e9f8eead8e5890208ca76',
                                'type' => 'request',
                                'desc' => '数据导出',
                                'method' => RequestMethod::POST,
                                'require_params' => [BuilderConst::ASYNC_TYPE_KEY => 'export'],
                            ],
                            [
                                'system_key' => 'd512e2cb846fe1b64697793869666a1d',
                                'type' => 'request',
                                'desc' => '数据导入',
                                'method' => RequestMethod::POST,
                                'require_params' => [BuilderConst::ASYNC_TYPE_KEY => 'import'],
                            ],
                        ],
                    ],
                    [
                        'system_key' => 'aba9659cd506eb51592828249ca4aa19',
                        'type' => 'route',
                        'name' => 'yunjDemoAllUserAdd',
                        'rule' => 'add',
                        'route' => class_route([UserAllController::class, 'add']),
                        
                        'desc' => '添加',
                        'items' => [
                            [
                                'system_key' => '57037fd22850233b3af745658745fccc',
                                'type' => 'request',
                                'desc' => '页面',
                                'method' => RequestMethod::GET,
                            ],
                            [
                                'system_key' => 'bf46cee1d6a2a27532ca14c03769c743',
                                'type' => 'request',
                                'desc' => '数据保存',
                                'method' => RequestMethod::POST,
                            ],
                        ],
                    ],
                    [
                        'system_key' => '084326234eac43f5afa4c14b8fbc6f22',
                        'type' => 'route',
                        'name' => 'yunjDemoAllUserEdit',
                        'rule' => 'edit',
                        'route' => class_route([UserAllController::class, 'edit']),
                        
                        'desc' => '编辑',
                        'items' => [
                            [
                                'system_key' => 'd81a6f47904c2446f12bc4c8bfd7ec7f',
                                'type' => 'request',
                                'desc' => '页面',
                                'method' => RequestMethod::GET,
                            ],
                            [
                                'system_key' => '7d032d38a377890b422b10ebff411614',
                                'type' => 'request',
                                'desc' => '数据保存',
                                'method' => RequestMethod::POST,
                            ],
                        ],
                    ],
                    [
                        'system_key' => '91df6cf51d73e1d943673087f6c0ceb7',
                        'type' => 'route',
                        'name' => 'yunjDemoAllUserImport',
                        'rule' => 'import',
                        'route' => class_route([UserAllController::class, 'import']),
                        
                        'desc' => '导入',
                        'items' => [
                            [
                                'system_key' => '3fa59a081688c1d04828675d7e9cdf7c',
                                'type' => 'request',
                                'desc' => '页面',
                                'method' => RequestMethod::GET,
                            ],
                            [
                                'system_key' => 'add01f4ffc0273b59610f964f290b493',
                                'type' => 'request',
                                'desc' => '数据处理',
                                'method' => RequestMethod::POST,
                            ],
                        ],
                    ],
                    [
                        'system_key' => 'b37c3c5f40c3357db0c95920b744b805',
                        'type' => 'route',
                        'name' => 'yunjDemoAllUserDropdownSearchOptions',
                        'rule' => 'user-dropdown-search-options',
                        'route' => class_route([UserAllController::class, 'userDropdownSearchOptions']),
                        
                        'desc' => '下拉搜索',
                        'items' => [
                            [
                                'system_key' => 'ca2002c7363ffa25cc9a8be6dd80e59e',
                                'type' => 'request',
                                'desc' => 'Demo 所有用户下拉搜索',
                            ]
                        ],
                    ],
                ]
            ],
        ]
    ]
];

// 处理demo标识
$requests = __handle_demo_make($requests);

return $requests;
