<?php

namespace yunj\app\demo\service\user;

use yunj\app\demo\enum\Sex;
use yunj\app\demo\enum\Grade;
use yunj\app\demo\enum\Hobby;
use yunj\app\demo\enum\State;
use yunj\app\demo\model\User as UserModel;
use yunj\app\demo\model\InterestCate as InterestCateModel;
use yunj\core\control\import\YunjImportRow;

final class Index {

    /**
     * @var UserModel
     */
    private $userModel;

    /**
     * @var InterestCateModel
     */
    private $interestCateModel;

    /**
     * @return UserModel
     */
    public function getUserModel(): UserModel {
        if (!$this->userModel) $this->setUserModel();
        return $this->userModel;
    }

    /**
     * @param UserModel $userModel
     */
    public function setUserModel(UserModel $userModel = null): void {
        $this->userModel = $userModel ?: (new UserModel());
    }

    /**
     * @return InterestCateModel
     */
    public function getInterestCateModel(): InterestCateModel {
        if (!$this->interestCateModel) $this->setInterestCateModel();
        return $this->interestCateModel;
    }

    /**
     * @param InterestCateModel $interestCateModel
     */
    public function setInterestCateModel(InterestCateModel $interestCateModel = null): void {
        $this->interestCateModel = $interestCateModel ?: (new InterestCateModel());
    }

    private static $instance;

    public static function getInstance() {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * Notes: 状态栏
     * Author: Uncle-L
     * Date: 2021/11/14
     * Time: 13:33
     * @return array
     */
    public function state(): array {
        return State::getTableState();
    }

    /**
     * Notes: 筛选表单
     * Author: Uncle-L
     * Date: 2021/11/15
     * Time: 11:59
     * @param $state
     * @return array
     */
    public function filter($state): array {
        $filter = [
            'name' => ['title' => '姓名'],
            'grade' => [
                'title' => '年级',
                'type' => 'select',
                'options' => [0 => '请选择'] + Grade::getTitleMap()
            ],
        ];
        return $filter;
    }

    /**
     * Notes: 头部工具栏
     * Author: Uncle-L
     * Date: 2021/11/15
     * Time: 14:36
     * @param $state
     * @return array
     */
    public function toolbar($state): array {
        $toolbar = ['add' => [
            'type' => 'openTab',
            'title' => '添加',
            'class' => 'layui-icon-add-circle',
            'url' => build_url('yunjDemoUserAdd'),
            'auth' => 'yunj_demo_user_add',
        ]];
        switch ($state) {
            case State::NORMAL:
                $toolbar += [
                    State::RECYLE_BIN => ['type' => 'asyncEvent', 'title' => '移入回收站', 'dropdown' => true, 'auth' => 'yunj_demo_user_recyle_bin',],
                ];
                break;
            case State::RECYLE_BIN:
                $toolbar += [
                    State::NORMAL => ['type' => 'asyncEvent', 'title' => '恢复正常', 'dropdown' => true, 'auth' => 'yunj_demo_user_normal'],
                    State::DELETED => ['type' => 'asyncEvent', 'title' => '永久删除', 'dropdown' => true, 'auth' => 'yunj_demo_user_deleted'],
                ];
                break;
        }
        return $toolbar;
    }

    /**
     * Notes: 头部右侧工具栏
     * Author: Uncle-L
     * Date: 2021/11/15
     * Time: 14:36
     * @param $state
     * @return array
     */
    public function defaultToolbar($state): array {
        return ['filter', 'print',
            'export' => [
                'auth' => 'yunj_demo_user_export'
            ],
            'import' => [
                'url' => build_url("yunjDemoUserImport"),
                'auth' => 'yunj_demo_user_import'
            ],
            'custom' => [
                'title' => '自定义前端事件',
                'class' => 'yunj-icon yunj-icon-pen',
            ]
        ];
    }

    /**
     * Notes: 表头
     * Author: Uncle-L
     * Date: 2021/11/14
     * Time: 13:42
     * @param $state [状态]
     * @return array
     */
    public function cols($state): array {
        $cols = [
            'id' => [
                'type' => 'checkbox',
                'fixed' => "left"
            ],
            'avatar' => [
                'title' => '头像',
                "templet" => "image",
                "align" => "center",
                "auth" => "yunj_demo_user_list_view_normal"
            ],
            'name' => [
                'title' => '姓名',
                "auth" => "yunj_demo_user_list_view_normal"
            ],
            'sex' => [
                'title' => '性别',
                "templet" => "enum",
                "options" => Sex::getTitleMap(),
                "align" => "center",
                "auth" => "yunj_demo_user_list_view_normal"
            ],
            'age' => [
                'title' => '年龄',
                "align" => "center",
                "auth" => "yunj_demo_user_list_view_normal"
            ],
            'grade' => [
                'title' => '年级',
                "templet" => "enum",
                "options" => Grade::getTitleMap(),
                "align" => "center",
                "auth" => "yunj_demo_user_list_view_normal"
            ],
            'color' => [
                'title' => '喜欢的颜色',
                "templet" => "color",
                "auth" => "yunj_demo_user_list_view_normal"
            ],
            "album" => [
                'title' => '相册',
                "templet" => "image",
                "auth" => "yunj_demo_user_list_view_album"
            ],
            "hobby" => [
                'title' => '爱好',
                "templet" => "enum",
                "options" => Hobby::getTitleMap(),
                "auth" => "yunj_demo_user_list_view_hobby"
            ],
            "interest_cate_ids" => [
                'title' => '兴趣分类',
                "templet" => "enum",
                "options" => $this->getInterestCateIdNameOptions(),
                "auth" => "yunj_demo_user_list_view_interest_cate_ids"
            ],
            'area' => [
                'title' => '地址',
                'templet' => 'area',
                "auth" => "yunj_demo_user_list_view_area"
            ],
            'document' => [
                'title' => '个人文档',
                'templet' => 'file',
                "auth" => "yunj_demo_user_list_view_document"
            ],
            'other_document' => [
                'title' => '其他文档',
                'templet' => 'file',
                "auth" => "yunj_demo_user_list_view_other_document"
            ],
            'test' => [
                'title' => '自定义表头',
                'align' => 'center',
                'hide' => 'yes',
                "auth" => "yunj_demo_user_list_view_test",
                'templet' => function () {
                    return "<div>自定义表头：{{ d.create_time}}</div>";
                }
            ],
            'create_time' => [
                'title' => '添加时间',
                'align' => 'center',
                'templet' => 'datetime',
                'hide' => 'yes',
                'sort' => true,
                "auth" => "yunj_demo_user_list_view_normal"
            ],
            'last_update_time' => [
                'title' => '上次更新时间',
                'align' => 'center',
                'templet' => 'datetime',
                'sort' => true,
                "auth" => "yunj_demo_user_list_view_normal"
            ],
            'action' => [
                'title' => '操作',
                'fixed' => "right",
                'templet' => 'action',
                'options' => [],
            ]
        ];
        $actionOptions = [
            'edit' => ['type' => 'openPopup', 'title' => '详情', 'icon' => 'layui-icon-survey', 'url' => build_url('yunjDemoUserEdit'), 'auth' => 'yunj_demo_user_edit_detail'],
        ];
        switch ($state) {
            case State::NORMAL:
                $actionOptions += [
                    State::RECYLE_BIN => ['type' => 'asyncEvent', 'title' => '移入回收站', 'dropdown' => true, 'auth' => 'yunj_demo_user_recyle_bin'],
                ];
                break;
            case State::RECYLE_BIN:
                $actionOptions += [
                    State::NORMAL => ['type' => 'asyncEvent', 'title' => '恢复正常', 'dropdown' => true, 'auth' => 'yunj_demo_user_normal'],
                    State::DELETED => ['type' => 'asyncEvent', 'title' => '永久删除', 'dropdown' => true, 'auth' => 'yunj_demo_user_deleted']
                ];
                break;
        }
        $cols['action']['options'] = $actionOptions;
        return $cols;
    }

    /**
     * Notes: 列表筛选条件where集合
     * Author: Uncle-L
     * Date: 2021/11/15
     * Time: 12:02
     * @param array $filter
     * @return array
     */
    private function listWhere(array $filter): array {
        $state = $filter['state'];
        $ids = $filter['ids'];
        $name = $filter['name'];
        $grade = $filter['grade'];
        $where = [];
        $where[] = $state ? ['state', '=', $state] : ['state', '<>', State::DELETED];
        if ($ids) $where[] = ['id', 'in', $ids];
        if ($name) $where[] = ['name', 'like', "%{$name}%"];
        if ($grade) $where[] = ['grade', '=', $grade];
        return $where;
    }

    /**
     * Notes: 数量
     * Author: Uncle-L
     * Date: 2021/11/14
     * Time: 13:50
     * @param array $filter [筛选条件]
     * @return int|string
     */
    public function count(array $filter) {
        $where = $this->listWhere($filter);
        return $this->getUserModel()->getOwnCount($where);
    }

    /**
     * Notes: 数据项
     * Author: Uncle-L
     * Date: 2021/11/15
     * Time: 12:05
     * @param int $page
     * @param int $pageSize
     * @param array $filter
     * @param array $sort
     * @return array|string
     */
    public function items(int $page, int $pageSize, array $filter, array $sort) {
        $where = $this->listWhere($filter);
        $order = $sort + ['id' => 'desc'];
        $items = $this->getUserModel()->getOwnRowsToArray($where, ["*"], $order, $page, $pageSize);
        foreach ($items as &$v) {
            $v['area'] = ['province' => $v['province'], 'city' => $v['city'], 'district' => $v['district']];
        }
        return $items;
    }

    /**
     * Notes: 异步事件
     * Author: Uncle-L
     * Date: 2021/11/15
     * Time: 15:02
     * @param $event
     * @param array $ids
     * @return bool|string
     */
    public function event($event, array $ids) {
        if (is_numeric($event)) $event = (int)$event;
        if (!State::isValue($event)) return "事件异常，请刷新页面尝试此操作";
        if (!is_positive_int_array($ids)) return "数据异常，请刷新页面尝试此操作";
        if (request_not_confirmed() && $event == State::RECYLE_BIN) {
            throw_confirm_json('确认移入回收站？');
        }
        if ($event == State::DELETED && strstr(request()->domain(), 'iyunj.cn')) return '演示环境不支持示例数据删除';
        $this->getUserModel()->change([
            "state" => $event,
            "last_update_time" => time(),
        ], [
            ["id", "in", $ids],
            ["state", "<>", State::DELETED],
        ]);
        return true;
    }

    /**
     * Notes: 获取所有兴趣分类数据项
     * Author: Uncle-L
     * Date: 2021/11/15
     * Time: 11:52
     * @return array
     */
    private function getAllInterestCateItems(): array {
        static $items;
        if (!$items) $items = $this->getInterestCateModel()->getOwnRowsToArray();
        return $items;
    }

    /**
     * Notes: 获取兴趣分类id和name的数组
     * Author: Uncle-L
     * Date: 2021/11/15
     * Time: 11:51
     * @return array
     */
    public function getInterestCateIdNameOptions(): array {
        static $options;
        if (!$options) {
            $items = $this->getAllInterestCateItems();
            $options = array_combine(array_column($items, "id"), array_column($items, "name"));
        }
        return $options;
    }

    /**
     * Notes: 切换栏
     * Author: Uncle-L
     * Date: 2021/11/15
     * Time: 16:34
     * @return array
     */
    public function tab(): array {
        return ["base" => "基础", "other" => "其他"];
    }

    /**
     * 新增字段
     * @param $tab
     * @return array
     */
    public function fieldAdd($tab): array {
        $field = [];
        switch ($tab) {
            case "base":
                $field = [
                    "avatar" => [
                        "title" => "头像",
                        "type" => "image",
                    ],
                    "name" => [
                        "title" => "姓名",
                        "type" => "text",
                        "default" => "某某人",
                        "verify" => "require|chsDash",
                        "desc" => '必须，允许键入汉字/字母/数字/下划线/破折号-',
                    ],
                    "sex" => [
                        "title" => "性别",
                        "type" => "radio",
                        "options" => Sex::getTitleMap(),
                        "verify" => "require",
                    ],
                    "age" => [
                        "title" => "年龄",
                        "default" => 18,
                        "verify" => "require|positiveInt",
                    ],
                    "grade" => [
                        "title" => "年级",
                        "type" => "select",
                        "options" => Grade::getTitleMap(),
                    ],
                    "hobby" => [
                        "title" => "爱好",
                        "type" => "checkbox",
                        "options" => Hobby::getTitleMap(),
                    ],
                    "area" => [
                        "title" => "地址",
                        "type" => "area",
                        "verify" => "require",
                    ],
                    "intro" => [
                        "title" => "个人介绍",
                        "type" => "textarea",
                        "default" => "某某人的个人介绍",
                        "verify" => "max:200",
                        "desc" => '限制200字符',
                    ]
                ];
                break;
            case "other":
                $field = [
                    "interest_cate_ids" => [
                        "title" => "感兴趣的分类",
                        'type' => 'tree',
                        'nodes' => $this->getAllInterestCateItems(),
                    ],
                    "friend_ids" => [
                        "title" => "朋友",
                        'type' => 'dropdownSearch',
                        'verify' => 'arrayPositiveInt',
                        'options' => build_url('yunjDemoUserDropdownSearchOptions'),
                    ],
                    "color" => [
                        "title" => "喜欢的颜色",
                        "type" => "color",
                    ],
                    "album" => [
                        "title" => "相册",
                        "type" => "image",
                        "multi" => true,
                    ],
                    'document' => [
                        'title' => '个人文档',
                        'type' => 'file',
                    ],
                    'other_document' => [
                        'title' => '其他文档',
                        'type' => 'file',
                        'multi' => true,
                    ],
                ];
                break;
        }
        return $field;
    }

    /**
     * 编辑字段
     * @param $tab
     * @return array
     */
    public function fieldEdit($tab): array {
        $field = [];
        switch ($tab) {
            case "base":
                $field = [
                    "id" => [
                        "title" => "用户",
                        "type" => "hidden",
                        "verify" => "require|positiveInt",
                    ],
                    "avatar" => [
                        "title" => "头像",
                        "type" => "image",
                        "auth" => "yunj_demo_user_edit_detail_normal",
                    ],
                    "name" => [
                        "title" => "姓名",
                        "type" => "text",
                        "default" => "某某人",
                        "verify" => "require|chsDash",
                        "desc" => '必须，允许键入汉字/字母/数字/下划线/破折号-',
                        "auth" => "yunj_demo_user_edit_detail_normal",
                    ],
                    "sex" => [
                        "title" => "性别",
                        "type" => "radio",
                        "options" => Sex::getTitleMap(),
                        "verify" => "require",
                        "auth" => "yunj_demo_user_edit_detail_normal",
                    ],
                    "age" => [
                        "title" => "年龄",
                        "default" => 18,
                        "verify" => "require|positiveInt",
                        "auth" => "yunj_demo_user_edit_detail_normal",
                    ],
                    "grade" => [
                        "title" => "年级",
                        "type" => "select",
                        "options" => Grade::getTitleMap(),
                        "auth" => "yunj_demo_user_edit_detail_normal",
                    ],
                    "hobby" => [
                        "title" => "爱好",
                        "type" => "checkbox",
                        "options" => Hobby::getTitleMap(),
                        "auth" => "yunj_demo_user_edit_detail_hobby",
                    ],
                    "area" => [
                        "title" => "地址",
                        "type" => "area",
                        "verify" => "require",
                        "auth" => "yunj_demo_user_edit_detail_area",
                    ],
                    "intro" => [
                        "title" => "个人介绍",
                        "type" => "textarea",
                        "default" => "某某人的个人介绍",
                        "verify" => "max:200",
                        "desc" => '限制200字符',
                        "auth" => "yunj_demo_user_edit_detail_normal",
                    ],
                    'test' => [
                        /** @var string $formId 当前表单构建器id */
                        'type' => function (string $formId) {
                            return "<label class='layui-form-label'>自定义字段标题</label>
                                    <div class='layui-input-inline yunj-form-item-control'>自定义字段内容：<span class='value'></span></div>
                                    <div class='yunj-form-item-desc'>自定义字段描述</div>
                                    <script type='application/javascript'>
                                        layui.use(['jquery'],function() {
                                            let $ = layui.jquery;
                                            
                                            // 设置字段值。事件名：yunj_form_{当前表单构建器id}_{字段key}_set_value
                                            $(document).bind('yunj_form_{$formId}_test_set_value',function(e,field,value) {
                                                // field.boxEl 当前字段外部父元素的jQuery对象
                                                // value 当前字段的值
                                                field.boxEl.find('.value').html(value);
                                            });
                                            // 获取字段值。事件名：yunj_form_{当前表单构建器id}_{字段key}_get_value
                                            $(document).bind('yunj_form_{$formId}_test_get_value',function(e,field,val) {
                                                // field.boxEl 当前字段外部父元素的jQuery对象
                                                // val.value 当前字段的值，默认为空字符串''
                                                val.value = field.boxEl.find('.value').html();
                                            });
                                            
                                        });
                                    </script>";
                        },
                        "auth" => "yunj_demo_user_edit_detail_test",
                    ]
                ];
                break;
            case "other":
                $field = [
                    "interest_cate_ids" => [
                        "title" => "感兴趣的分类",
                        'type' => 'tree',
                        'nodes' => $this->getAllInterestCateItems(),
                        "auth" => "yunj_demo_user_edit_detail_interest_cate_ids",
                    ],
                    "friend_ids" => [
                        "title" => "朋友",
                        'type' => 'dropdownSearch',
                        'verify' => 'arrayPositiveInt',
                        'options' => build_url('yunjDemoUserDropdownSearchOptions'),
                        "auth" => "yunj_demo_user_edit_detail_friend_ids",
                    ],
                    "color" => [
                        "title" => "喜欢的颜色",
                        "type" => "color",
                        "auth" => "yunj_demo_user_edit_detail_color",
                    ],
                    "album" => [
                        "title" => "相册",
                        "type" => "image",
                        "multi" => true,
                        "auth" => "yunj_demo_user_edit_detail_album",
                    ],
                    'document' => [
                        'title' => '个人文档',
                        'type' => 'file',
                        "auth" => "yunj_demo_user_edit_detail_document",
                    ],
                    'other_document' => [
                        'title' => '其他文档',
                        'type' => 'file',
                        'multi' => true,
                        "auth" => "yunj_demo_user_edit_detail_other_document",
                    ],
                ];
                break;
        }
        if (!admin_member_auth_check('yunj_demo_user_edit_submit')) {
            // 没有编辑提交权限，全部字段改为只读
            foreach ($field as &$v) {
                $v['readonly'] = true;
            }
        }
        return $field;
    }

    /**
     * Notes: 数据获取
     * Author: Uncle-L
     * Date: 2021/11/16
     * Time: 10:45
     * @param int|null $id
     * @return array|string
     */
    public function load(int $id = null) {
        $id = $id ?: input("get.id/d");
        $data = $this->getUserModel()->getOwnRowToArray([
            ["id", "=", $id],
            ["state", "<>", State::DELETED],
        ]);
        if (!$data) return [];
        $data['area'] = ['province' => $data['province'], 'city' => $data['city'], 'district' => $data['district']];
        $data['test'] = '这是测试内容';
        return $data;
    }

    /**
     * Notes: 表单提交
     * Author: Uncle-L
     * Date: 2021/11/15
     * Time: 17:39
     * @param array $data
     * @param false $isEdit
     * @return bool
     */
    public function submit(array $data, $isEdit = false): bool {
        $saveData = $this->submitDataHandle($data);
        if (!$saveData) {
            return false;
        }
        $currTime = time();
        if ($isEdit) {
            $saveData["id"] = $data["id"];
            $saveData["last_update_time"] = $currTime;
            $res = $this->getUserModel()->change($saveData);
        } else {
            $saveData["create_time"] = $currTime;
            $saveData["last_update_time"] = $currTime;
            $res = $this->getUserModel()->addRow($saveData);
        }
        return !!$res;
    }

    // 表单提交数据处理
    private function submitDataHandle(array $data): array {
        $saveData = [];
        if (isset($data['avatar'])) {
            $saveData['avatar'] = $data['avatar'];
        }
        if (isset($data['name'])) {
            $saveData['name'] = $data['name'];
        }
        if (isset($data['sex'])) {
            $saveData['sex'] = $data['sex'];
        }
        if (isset($data['age'])) {
            $saveData['age'] = $data['age'];
        }
        if (isset($data['grade'])) {
            $saveData['grade'] = $data['grade'];
        }
        if (isset($data['intro'])) {
            $saveData['intro'] = $data['intro'];
        }
        if (isset($data['hobby'])) {
            $saveData['hobby'] = $data['hobby'] ? implode(',', $data['hobby']) : "";
        }
        if (isset($data['area'])) {
            $saveData['province'] = $data['area'] && isset($data['area']['province']) ? $data['area']['province'] : '';
            $saveData['city'] = $data['area'] && isset($data['area']['city']) ? $data['area']['city'] : '';
            $saveData['district'] = $data['area'] && isset($data['area']['district']) ? $data['area']['district'] : '';
        }
        if (isset($data['interest_cate_ids'])) {
            $saveData['interest_cate_ids'] = $data['interest_cate_ids'] ? json_encode($data['interest_cate_ids']) : '';
        }
        if (isset($data['friend_ids'])) {
            $saveData['friend_ids'] = $data['friend_ids'] ? json_encode($data['friend_ids']) : '';
        }
        if (isset($data['color'])) $saveData['color'] = $data['color'];
        if (isset($data['album'])) {
            $saveData['album'] = $data['album'] ? implode(',', $data['album']) : "";
        }
        if (isset($data['document'])) {
            $saveData['document'] = $data['document'];
        }
        if (isset($data['other_document'])) {
            $saveData['other_document'] = $data['other_document'] ? json_encode($data['other_document']) : '';
        }
        return $saveData;
    }

    /**
     * Notes: 用户下拉搜索
     * Author: Uncle-L
     * Date: 2021/11/15
     * Time: 17:47
     * @param array $param
     * @return array
     */
    public function userDropdownSearchOptions(array $param = []): array {
        $param = $param ?: input('get.');
        $ids = filter_sql($param['ids']);
        $keywords = filter_sql($param['keywords']);

        $where = [];
        if ($ids) $where[] = ['id', 'in', $ids];
        if ($keywords) $where[] = ['name', 'like', "%{$keywords}%"];
        $field = ['id', 'name'];
        $items = $this->getUserModel()->getOwnRowsToArray($where, $field, ["id" => "desc"], 0, 20);
        return $items;
    }

    /**
     * Notes: 工作表
     * Author: Uncle-L
     * Date: 2021/11/16
     * Time: 13:45
     * @return array
     */
    public function sheet(): array {
        return ["一年级", "二年级", "三年级", "四年级"];
    }

    /**
     * Notes: 导入表头
     * Author: Uncle-L
     * Date: 2021/11/14
     * Time: 13:42
     * @param $sheet [工作表]
     * @return array
     */
    public function importCols($sheet): array {
        $cols = [
            'name' => [
                "title" => '姓名',
                "default" => "某某人",
                "verify" => "require|chsDash|nameUnique",
                "desc" => '必填，允许输入汉字/字母/数字/下划线/破折号-'
            ],
            'sex' => [
                "title" => '性别',
                'default' => '男',
                'verify' => 'require|in:男,女',
                "desc" => "必填，男/女",
            ],
            'age' => [
                "title" => '年龄',
                'default' => '18',
                'verify' => 'require|positiveInt',
                "desc" => "必填，正整数",
            ],
            "hobby" => [
                "title" => "爱好",
                'default' => '阅读',
                'verify' => 'in:写作,阅读',
                "desc" => "写作/阅读",
            ],
            'intro' => [
                "title" => '简介',
                'default' => '个人简介',
                'verify' => 'max:200',
                "desc" => "限制200字符，如：姓名、曾用名、出生日期、籍贯、出生地、民族、现有文化程度、家庭现住址、现在工作单位和担任的社会职务、有何专业技术职称等基本情况。",
            ],
        ];
        return $cols;
    }

    /**
     * Notes: 多行数据处理
     * Author: Uncle-L
     * Date: 2021/11/16
     * Time: 14:27
     * @param array<YunjImportRow> $rows
     * @return  mixed|string|array
     */
    public function rows(array $rows) {
        $datas = [];
        $currTime = time();
        $error = [];
        foreach ($rows as $row) {
            $rowData = $row->getData();
            $sheet = $row->getSheet();
            $datas[] = [
                "name" => $rowData["name"],
                "sex" => Sex::getValueByTitle($rowData["sex"]),
                "age" => $rowData["age"],
                "grade" => Grade::getValueByTitle($sheet),
                "hobby" => Hobby::getValueByTitle($rowData["hobby"]),
                "intro" => $rowData["intro"],
                'create_time' => $currTime,
                'last_update_time' => $currTime,
            ];
        }
        if ($datas) $this->getUserModel()->addRows($datas);
        sleep(1);
        return $error;
    }

}