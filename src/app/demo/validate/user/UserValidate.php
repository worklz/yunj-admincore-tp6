<?php

namespace yunj\app\demo\validate\user;

use yunj\app\demo\model\User as UserModel;
use yunj\app\demo\validate\Validate;

class UserValidate extends Validate {

    protected function nameUnique($value, string $rule, array $data, string $field, string $title) {
        $model = new UserModel();
        if ($model->getOwnCount([['name', '=', $value]])) {
            return "姓名已存在";
        }
        return true;
    }

}