<?php

namespace yunj\app\demo\controller\view;

use yunj\app\demo\controller\Controller;

class IndexController extends Controller {

    public function instruction() {
        return $this->view("view/index/instruction");
    }

    public function icon() {
        return $this->view("view/index/icon");
    }

}