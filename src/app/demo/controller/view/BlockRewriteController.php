<?php

namespace yunj\app\demo\controller\view;

use yunj\app\demo\controller\Controller;

class BlockRewriteController extends Controller {

    public function seo() {
        return $this->view("view/block_rewrite/seo");
    }

    public function headStyle() {
        return $this->view("view/block_rewrite/head_style");
    }

    public function headScript() {
        return $this->view("view/block_rewrite/head_script");
    }

    public function content() {
        return $this->view("view/block_rewrite/content");
    }

    public function script() {
        return $this->view("view/block_rewrite/script");
    }

}