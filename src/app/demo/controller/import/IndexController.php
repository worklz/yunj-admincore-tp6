<?php

namespace yunj\app\demo\controller\import;

use yunj\app\demo\controller\Controller;
use yunj\app\demo\service\import\Index as Service;
use yunj\core\builder\YunjImport;

class IndexController extends Controller {

    // 简单示例
    public function sample() {
        $builder = YI('demo')
            ->cols(function (YunjImport $builder, $sheet) {
                return [
                    'name'=>[
                        "title"=>'姓名',
                        "default"=>"小王",
                        "verify"=>'require|chs',
                        "desc"=>"必填",
                    ],
                    'age'=>[
                        "title"=>'年龄',
                        "default"=>18,
                        "verify"=>'require|positiveInt',
                        "desc"=>"必填，必须为正整数",
                    ],
                    'sex'=>[
                        "title"=>'性别',
                        "default"=>"男",
                        "verify"=>"require|in:男,女",
                        "desc"=>"必填，可选值：男/女",
                    ]
                ];
            })
            ->row(function (YunjImport $builder, $row) {
                // 导入成功（可不设置，默认为成功）
                //$row->importSuccess();
                // 导入失败
                $row->importFail('演示数据不能进行导入操作');
            });
        return view_import($builder);
    }

    public function chain() {
        $builder = YI('demo')
            ->sheet(Service::getInstance()->sheet())
            ->cols(function (YunjImport $builder, $sheet) {
                return Service::getInstance()->cols($sheet);
            })
            ->row(function (YunjImport $builder, $row) {
                return "演示数据不能进行导入操作";
            });
        $builder->assign();
        return $this->view("import/index/chain");
    }

    public function arrayConfig() {
        $builder = YI('demo', [
            "sheet" => Service::getInstance()->sheet(),
            "cols" => function (YunjImport $builder, $sheet) {
                return Service::getInstance()->cols($sheet);
            },
            "row" => function (YunjImport $builder, $row) {
                return "演示数据不能进行导入操作";
            }
        ]);
        $builder->assign();
        return $this->view("import/index/array_config");
    }

}