<?php

namespace yunj\app\demo\controller\user;

use yunj\app\demo\enum\Sex;
use yunj\app\demo\enum\Grade;
use yunj\app\demo\enum\Hobby;
use yunj\app\demo\controller\Controller;
use yunj\app\demo\service\user\All as Service;
use yunj\core\builder\YunjForm;
use yunj\core\builder\YunjImport;
use yunj\core\builder\YunjTable;

class AllController extends Controller {

    /**
     * @return \think\response\View
     * @throws \yunj\core\exception\GeneralException
     */
    public function lists() {
        $builder = YT("demo")
            ->filter(['name' => ['title' => '姓名']])
            ->defaultToolbar([
                'filter', 'print',
                'export' => ['auth' => 'yunj_demo_all_user_export'],
                'import' => [
                    'auth' => 'yunj_demo_all_user_import',
                    'url' => build_url("yunjDemoAllUserImport")
                ]
            ])
            ->cols([
                'id' => [
                    'type' => 'checkbox',
                    'fixed' => "left"
                ],
                'avatar' => [
                    'title' => '头像',
                    "templet" => "image",
                    "align" => "center",
                    "auth" => "yunj_demo_all_user_list_view_normal"
                ],
                'name' => ['title' => '姓名'],
                'sex' => [
                    'title' => '性别',
                    "templet" => "enum",
                    "options" => Sex::getTitleMap(),
                    "align" => "center",
                    "auth" => "yunj_demo_all_user_list_view_normal"
                ],
                'age' => [
                    'title' => '年龄',
                    "align" => "center",
                    "auth" => "yunj_demo_all_user_list_view_normal"
                ],
                'grade' => [
                    'title' => '年级',
                    "templet" => "enum",
                    "options" => Grade::getTitleMap(),
                    "align" => "center",
                    "auth" => "yunj_demo_all_user_list_view_normal"
                ],
                'color' => [
                    'title' => '喜欢的颜色',
                    "templet" => "color",
                    "auth" => "yunj_demo_all_user_list_view_normal"
                ],
                "album" => [
                    'title' => '相册',
                    "templet" => "image",
                    "auth" => "yunj_demo_all_user_list_view_album"
                ],
                "hobby" => [
                    'title' => '爱好',
                    "templet" => "enum",
                    "options" => Hobby::getTitleMap(),
                    "auth" => "yunj_demo_all_user_list_view_hobby"
                ],
                "interest_cate_ids" => [
                    'title' => '兴趣分类',
                    "templet" => "enum",
                    "options" => Service::getInstance()->getInterestCateIdNameOptions(),
                    "auth" => "yunj_demo_all_user_list_view_interest_cate_ids"
                ],
                'area' => [
                    'title' => '地址',
                    'templet' => 'area',
                    'hide' => 'yes',
                    "auth" => "yunj_demo_all_user_list_view_area"
                ],
                'document' => [
                    'title' => '个人文档',
                    'templet' => 'file',
                    'hide' => 'yes',
                    "auth" => "yunj_demo_all_user_list_view_document"
                ],
                'other_document' => [
                    'title' => '其他文档',
                    'templet' => 'file',
                    'hide' => 'yes',
                    "auth" => "yunj_demo_all_user_list_view_other_document"
                ],
                'create_time' => [
                    'title' => '添加时间',
                    'align' => 'center',
                    'templet' => 'datetime',
                    'hide' => 'yes',
                    "auth" => "yunj_demo_all_user_list_view_normal"
                ],
                'last_update_time' => [
                    'title' => '上次更新时间',
                    'align' => 'center',
                    'templet' => 'datetime',
                    "auth" => "yunj_demo_all_user_list_view_normal"
                ],
                'action' => [
                    'title' => '操作',
                    'fixed' => "right",
                    'templet' => 'action',
                    'options' => [
                        'edit' => ['type' => 'openPopup', 'title' => '详情', 'icon' => 'layui-icon-survey', 'url' => build_url('yunjDemoAllUserEdit'), 'auth' => 'yunj_demo_all_user_edit_detail'],
                    ],
                ]
            ])
            ->count(function (YunjTable $builder, $filter) {
                return Service::getInstance()->count($filter);
            })
            ->items(function (YunjTable $builder, $page, $pageSize, $filter, $sort) {
                return Service::getInstance()->items($page, $pageSize, $filter, $sort);
            });
        return view_table($builder);
    }

    /**
     * @return \think\response\View
     * @throws \yunj\core\exception\GeneralException
     */
    public function add() {
        $builder = YF('demo')
            ->field([
                "avatar" => [
                    "title" => "头像",
                    "type" => "image",
                ],
                "name" => [
                    "title" => "姓名",
                    "type" => "text",
                    "default" => "某某人",
                    "verify" => "require|chsDash",
                    "desc" => '必须，允许键入汉字/字母/数字/下划线/破折号-'
                ],
                "sex" => [
                    "title" => "性别",
                    "type" => "radio",
                    "options" => Sex::getTitleMap(),
                    "verify" => "require",
                ],
                "age" => [
                    "title" => "年龄",
                    "default" => 18,
                    "verify" => "require|positiveInt",
                ],
                "grade" => [
                    "title" => "年级",
                    "type" => "select",
                    "options" => Grade::getTitleMap(),
                ],
                "hobby" => [
                    "title" => "爱好",
                    "type" => "checkbox",
                    "options" => Hobby::getTitleMap()
                ],
                "area" => [
                    "title" => "地址",
                    "type" => "area",
                    "verify" => "require",
                ],
                "intro" => [
                    "title" => "个人介绍",
                    "type" => "textarea",
                    "default" => "某某人的个人介绍",
                    "verify" => "max:200",
                    "desc" => '限制200字符'
                ],
                "interest_cate_ids" => [
                    "title" => "感兴趣的分类",
                    'type' => 'tree',
                    'nodes' => Service::getInstance()->getAllInterestCateItems()
                ],
                "friend_ids" => [
                    "title" => "朋友",
                    'type' => 'dropdownSearch',
                    'verify' => 'arrayPositiveInt',
                    'options' => build_url('yunjDemoAllUserDropdownSearchOptions'),
                ],
                "color" => [
                    "title" => "喜欢的颜色",
                    "type" => "color",
                ],
                "album" => [
                    "title" => "相册",
                    "type" => "image",
                    "multi" => true,
                ],
                'document' => [
                    'title' => '个人文档',
                    'type' => 'file',
                ],
                'other_document' => [
                    'title' => '其他文档',
                    'type' => 'file',
                    'multi' => true,
                ],
            ])
            ->button('reload', 'reset', 'submit')
            ->submit(function (YunjForm $builder, $data) {
                $res = Service::getInstance()->submit($data);
                return $res ? success_json(['reload' => true]) : error_json();
            });
        return view_form($builder);
    }

    /**
     * @return \think\response\View
     * @throws \yunj\core\exception\GeneralException
     */
    public function edit() {
        $builder = YF('demo')
            ->field([
                "id" => [
                    "title" => "id",
                    "type" => "hidden",
                    "verify" => "require|positiveInt",
                ],
                "avatar" => [
                    "title" => "头像",
                    "type" => "image",
                    'auth' => 'yunj_demo_data_all_user_list_col_normal'
                ],
                "name" => [
                    "title" => "姓名",
                    "type" => "text",
                    "default" => "某某人",
                    "verify" => "require|chsDash",
                    "desc" => '必须，允许键入汉字/字母/数字/下划线/破折号-',
                    'auth' => 'yunj_demo_data_all_user_list_col_normal'
                ],
                "sex" => [
                    "title" => "性别",
                    "type" => "radio",
                    "options" => Sex::getTitleMap(),
                    "verify" => "require",
                    'auth' => 'yunj_demo_data_all_user_list_col_normal'
                ],
                "age" => [
                    "title" => "年龄",
                    "default" => 18,
                    "verify" => "require|positiveInt",
                    'auth' => 'yunj_demo_data_all_user_list_col_normal'
                ],
                "grade" => [
                    "title" => "年级",
                    "type" => "select",
                    "options" => Grade::getTitleMap(),
                    'auth' => 'yunj_demo_data_all_user_list_col_normal'
                ],
                "hobby" => [
                    "title" => "爱好",
                    "type" => "checkbox",
                    "options" => Hobby::getTitleMap(),
                    'auth' => 'yunj_demo_data_all_user_list_col_hobby'
                ],
                "area" => [
                    "title" => "地址",
                    "type" => "area",
                    "verify" => "require",
                    'auth' => 'yunj_demo_data_all_user_list_col_area'
                ],
                "intro" => [
                    "title" => "个人介绍",
                    "type" => "textarea",
                    "default" => "某某人的个人介绍",
                    "verify" => "max:200",
                    "desc" => '限制200字符',
                    'auth' => 'yunj_demo_data_all_user_list_col_intro'
                ],
                "interest_cate_ids" => [
                    "title" => "感兴趣的分类",
                    'type' => 'tree',
                    'nodes' => Service::getInstance()->getAllInterestCateItems(),
                    'auth' => 'yunj_demo_data_all_user_list_col_interest_cate_ids'
                ],
                "friend_ids" => [
                    "title" => "朋友",
                    'type' => 'dropdownSearch',
                    'verify' => 'arrayPositiveInt',
                    'auth' => 'yunj_demo_data_all_user_list_col_friend_ids',
                    'options' => build_url('yunjDemoAllUserDropdownSearchOptions'),
                ],
                "color" => [
                    "title" => "喜欢的颜色",
                    "type" => "color",
                    'auth' => 'yunj_demo_data_all_user_list_col_color',
                ],
                "album" => [
                    "title" => "相册",
                    "type" => "image",
                    "multi" => true,
                    'auth' => 'yunj_demo_data_all_user_list_col_album',
                ],
                'document' => [
                    'title' => '个人文档',
                    'type' => 'file',
                    'auth' => 'yunj_demo_data_all_user_list_col_document',
                ],
                'other_document' => [
                    'title' => '其他文档',
                    'type' => 'file',
                    'multi' => true,
                    'auth' => 'yunj_demo_data_all_user_list_col_other_document',
                ],
            ])
            ->button(function () {
                return admin_member_auth_check('yunj_demo_all_user_edit_submit') ? ['reload', 'reset', 'submit'] : [];
            })
            ->load(function () {
                return Service::getInstance()->load();
            })
            ->submit(function (YunjForm $builder, $data) {
                $res = Service::getInstance()->submit($data, true);
                return $res ? success_json(['reload' => true]) : error_json("修改失败");
            });
        return view_form($builder);
    }

    /**
     * @return \think\response\View
     * @throws \yunj\core\exception\GeneralException
     */
    public function import() {
        $builder = YI('demo', [
            "cols" => function () {
                $cols = [
                    'name' => [
                        "title" => '姓名',
                        "default" => "某某人",
                        "verify" => "require|chsDash",
                        "desc" => '必填，允许输入汉字/字母/数字/下划线/破折号-'
                    ],
                    'sex' => [
                        "title" => '性别',
                        'default' => '男',
                        'verify' => 'require|in:男,女',
                        "desc" => "必填，男/女",
                    ],
                    'grade' => [
                        "title" => '年级',
                        'default' => '一年级',
                        'verify' => 'require|in:' . implode(",", Grade::getTitleMap()),
                        "desc" => "必填，" . implode("/", Grade::getTitleMap()),
                    ],
                    'age' => [
                        "title" => '年龄',
                        'default' => '18',
                        'verify' => 'require|positiveInt',
                        "desc" => "必填，正整数",
                    ],
                    "hobby" => [
                        "title" => "爱好",
                        'default' => '阅读',
                        'verify' => 'in:写作,阅读',
                        "desc" => "写作/阅读",
                    ],
                    'intro' => [
                        "title" => '简介',
                        'default' => '个人简介',
                        'verify' => 'max:200',
                        "desc" => "限制200字符，如：姓名、曾用名、出生日期、籍贯、出生地、民族、现有文化程度、家庭现住址、现在工作单位和担任的社会职务、有何专业技术职称等基本情况。",
                    ],
                ];
                return $cols;
            },
            "rows" => function (YunjImport $builder, $rows) {
                return Service::getInstance()->rows($rows);
            }
        ]);
        return view_import($builder);
    }

    public function userDropdownSearchOptions() {
        $items = Service::getInstance()->userDropdownSearchOptions();
        return success_json($items);
    }

}