<?php

namespace yunj\app\demo\controller\user;

use yunj\app\demo\controller\Controller;
use yunj\app\demo\service\user\Index as Service;
use yunj\app\demo\validate\user\UserValidate;
use yunj\core\builder\YunjForm;
use yunj\core\builder\YunjImport;
use yunj\core\builder\YunjTable;

class IndexController extends Controller {

    public function lists() {
        $builder = YT('demo', [
            "state" => Service::getInstance()->state(),
            "filter" => function (YunjTable $builder, $state) {
                return Service::getInstance()->filter($state);
            },
            "toolbar" => function (YunjTable $builder, $state) {
                return Service::getInstance()->toolbar($state);
            },
            "defaultToolbar" => function (YunjTable $builder, $state) {
                return Service::getInstance()->defaultToolbar($state);
            },
            "cols" => function (YunjTable $builder, $state) {
                return Service::getInstance()->cols($state);
            },
            "count" => function (YunjTable $builder, $filter) {
                return Service::getInstance()->count($filter);
            },
            "items" => function (YunjTable $builder, $page, $pageSize, $filter, $sort) {
                return Service::getInstance()->items($page, $pageSize, $filter, $sort);
            },
            "event" => function (YunjTable $builder, $event, $ids) {
                $res = Service::getInstance()->event($event, $ids);
                return $res === true ? success_json(['reload' => true]) : error_json($res);
            }
        ]);
        $builder->assign();
        return $this->view("user/index/lists");
    }

    public function add() {
        $builder = YF('demo', [
            "tab" => Service::getInstance()->tab(),
            "field" => function (YunjForm $builder, $tab) {
                return Service::getInstance()->fieldAdd($tab);
            },
            "button" => ['reload', 'reset', 'submit'],
            "submit" => function (YunjForm $builder, $data) {
                $res = Service::getInstance()->submit($data);
                return $res ? success_json(['reload' => true]) : error_json();
            },
        ]);
        $builder->assign();
        return $this->view("user/index/add");
    }

    public function edit() {
        $builder = YF('demo', [
            "tab" => Service::getInstance()->tab(),
            "field" => function (YunjForm $builder, $tab) {
                return Service::getInstance()->fieldEdit($tab);
            },
            //"button" => ['reload'=>['auth'=>'yunj_demo_user_edit_submit'],'reset'=>['auth'=>'yunj_demo_user_edit_submit'], 'submit'=>['auth'=>'yunj_demo_user_edit_submit']],
            'button' => function () {
                return admin_member_auth_check('yunj_demo_user_edit_submit')
                    ? ['custom' => ['desc' => '自定义按钮'], 'back', 'reload', 'reset', 'submit'] : [];
            },
            "load" => function () {
                return Service::getInstance()->load();
            },
            "submit" => function (YunjForm $builder, $data) {
                if (request_not_confirmed()) {
                    return confirm_json('确认提交？');
                }
                $res = Service::getInstance()->submit($data, true);
                return $res ? success_json(['reload' => true]) : error_json("修改失败");
            },
        ]);
        $builder->assign();
        return $this->view("user/index/edit");
    }

    public function userDropdownSearchOptions() {
        $items = Service::getInstance()->userDropdownSearchOptions();
        return success_json($items);
    }

    public function import() {
        $builder = YI('demo', [
            "sheet" => Service::getInstance()->sheet(),
            "cols" => function (YunjImport $builder, $sheet) {
                return Service::getInstance()->importCols($sheet);
            },
            "validate" => UserValidate::class,
            "rows" => function (YunjImport $builder, $rows) {
                return Service::getInstance()->rows($rows);
            },
            "limit" => 1
        ]);
        $builder->assign();
        return $this->view("user/index/import");
    }

}