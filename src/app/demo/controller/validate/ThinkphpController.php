<?php

namespace yunj\app\demo\controller\validate;

use yunj\app\demo\controller\Controller;
use yunj\app\demo\service\validate\Thinkphp as Service;

class ThinkphpController extends Controller {

    public function index() {
        if (request()->isAjax()) {
            $res = Service::getInstance()->handleData();
            return $res === true ? success_json() : error_json($res);
        }
        return $this->view("validate/thinkphp/index");
    }

}