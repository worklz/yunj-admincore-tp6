<?php

namespace yunj\app\demo\controller;

class IndexController extends Controller {

    public function welcome() {
        return $this->view("index/welcome");
    }

}