<?php

namespace yunj\app\demo\controller\table;

use yunj\app\admin\enum\TableBuilderEvent;
use yunj\app\demo\controller\Controller;
use yunj\app\demo\enum\State;
use yunj\app\demo\service\table\Index as Service;
use yunj\core\builder\YunjTable;

class IndexController extends Controller {

    // 简单示例
    public function sample() {
        $builder = YT('demo')
            ->filter(function (YunjTable $builder, $state) {
                return ['name' => ['title' => '姓名']];
            })
            ->defaultToolbar(function (YunjTable $builder, $state) {
                return [
                    'import' => build_url("yunjDemoImportBuilderSample")
                ];
            })
            ->cols(function (YunjTable $builder, $state) {
                return [
                    'id' => ['type' => 'checkbox'],
                    'name' => ['title' => '姓名'],
                    'age' => ['title' => '年龄'],
                    'sex' => ['title' => '性别', "templet" => "enum", "options" => ['male' => '男', 'female' => '女']],
                    'action' => ['title' => '操作', 'templet' => 'action', 'options' => [
                        'edit' => ['type' => 'openPopup', 'title' => '详情', 'class' => 'layui-icon-survey', 'url' => build_url('yunjDemoFormBuilderSample')]
                    ]]
                ];
            })
            ->count(function (YunjTable $builder, $filter) {
                // ...查询当前条件下的数据条数
                return 1;
            })
            ->items(function (YunjTable $builder, $page, $pageSize, $filter, $sort) {
                return [
                    ['id' => 999, 'name' => '小王', 'age' => 18, 'sex' => 'male']
                ];
            })
            ->event(function (YunjTable $builder, $event, $ids) {
                return error_json("演示数据不能进行操作");
            });
        return view_table($builder);
    }

    public function chain() {
        $builder = YT('demo')
            ->state(Service::getInstance()->state())
            ->page(false)
            ->defaultToolbar(function (YunjTable $builder, $state){
                return ["filter"];
            })
            ->cols(function (YunjTable $builder, $state) {
                return Service::getInstance()->cols($state);
            })
            ->count(function (YunjTable $builder, $filter) {
                return Service::getInstance()->count($filter);
            })
            ->items(function (YunjTable $builder, $page, $pageSize, $filter, $sort) {
                return Service::getInstance()->items($page, $pageSize, $filter, $sort);
            })
            ->event(function (YunjTable $builder, $event, $ids) {
                return error_json("演示数据不能进行操作");
            });
        $builder->assign();
        return $this->view("table/index/chain");
    }

    public function arrayConfig() {
        $builder = YT('demo', [
            "state" => Service::getInstance()->state(),
            "page" => false,
            "defaultToolbar"=> function (YunjTable $builder, $state) {
                return ["filter"];
            },
            "cols" => function (YunjTable $builder, $state) {
                return Service::getInstance()->cols($state);
            },
            "count" => function (YunjTable $builder, $filter) {
                return Service::getInstance()->count($filter);
            },
            "items" => function (YunjTable $builder, $page, $pageSize, $filter, $sort) {
                return Service::getInstance()->items($page, $pageSize, $filter, $sort);
            },
            "event" => function (YunjTable $builder, $event, $ids) {
                return error_json("演示数据不能进行操作");
            }
        ]);
        $builder->assign();
        return $this->view("table/index/array_config");
    }

    public function tree() {
        $builder = YT('demo', [
            "state" => [
                ['key'=>1,'title'=>'正常'],
                ['key'=>2,'title'=>'回收站'],
            ],
            "page" => function (YunjTable $builder, $state) {
                return $state !== 1;
            },
            "tree" => function (YunjTable $builder, $state) {
                return $state === 1;
            },
            "cols" => function (YunjTable $builder, $state) {
                return [
                    'name' => ['title' => '名称'],
                    'state' => [
                        'title' => '状态',
                        "templet" => "enum",
                        "options" => [
                            2 => "冻结",
                            1 => "正常",
                        ]
                    ],
                    'action' => [
                        'title' => '操作',
                        'templet' => 'action',
                        'options' => [
                            'edit' => ['type' => 'openPopup', 'title' => '详情', 'class' => 'layui-icon-survey', 'url' => '....'],
                            State::NORMAL => ['type' => 'asyncEnent', 'title' => '还原', 'dropdown' => true],
                            State::RECYLE_BIN => ['type' => 'asyncEnent', 'title' => '移入回收站', 'dropdown' => true],
                            State::DELETED => ['type' => 'asyncEnent', 'title' => '永久删除', 'dropdown' => true],
                        ]
                    ]
                ];
            },
            "count" => function (YunjTable $builder, $filter) {
                return 10;
            },
            "items" => function (YunjTable $builder, $page, $pageSize, $filter, $sort) {
                $items = [
                    ["id" => 1, "pid" => 0, "name" => "男装"],
                    ["id" => 2, "pid" => 1, "name" => "卫衣"],
                    ["id" => 3, "pid" => 2, "name" => "白色"],
                    ["id" => 4, "pid" => 1, "name" => "衬衫"],
                    ["id" => 5, "pid" => 0, "name" => "运动"],
                    ["id" => 6, "pid" => 5, "name" => "鞋子"],
                    ["id" => 7, "pid" => 5, "name" => "帽子"],
                    ["id" => 8, "pid" => 7, "name" => "太阳帽"],
                    ["id" => 9, "pid" => 8, "name" => "遮阳帽"],
                    ["id" => 10, "pid" => 0, "name" => "百货"],
                ];
                foreach ($items as &$item) {
                    $item['state'] = $filter['state'];
                }
                return $items;
            },
            "event" => function (YunjTable $builder, $event, $ids) {
                return error_json("演示数据不能进行操作");
            }
        ]);
        $builder->assign();
        return view_table($builder);
    }

    public function dragSort() {
        $builder = YT('demo', [
            "state" => Service::getInstance()->state(),
            "page" => false,
            "cols" => function (YunjTable $builder, $state) {
                return Service::getInstance()->cols($state, true);
            },
            "count" => function (YunjTable $builder, $filter) {
                return Service::getInstance()->count($filter);
            },
            "items" => function (YunjTable $builder, $page, $pageSize, $filter, $sort) {
                return Service::getInstance()->items($page, $pageSize, $filter, $sort);
            },
            "event" => function (YunjTable $builder, $event, $ids) {
                return error_json("演示数据不能进行操作");
            }
        ]);
        $builder->assign();
        return $this->view("table/index/drag_sort");
    }

    public function pk() {
        $builder = YT('demo', [
            "pk" => function (YunjTable $builder, $state) {
                return "code";
            },
            "cols" => function (YunjTable $builder, $state) {
                return [
                    'code' => ['type' => 'checkbox'],
                    'name' => ['title' => '名称', 'templet' => 'dragSort'],
                    'attr' => ['title' => '属性'],
                    'action' => [
                        'title' => '操作',
                        'templet' => 'action',
                        'options' => [
                            'back_get_pk' => [
                                'title' => '后端获取选定数据的主键',
                                'type' => 'asyncEvent'
                            ],
                            'front_get_pk' => [
                                'title' => '前端获取选定数据的主键'
                            ],
                            'open_tab_page' => [
                                'title' => '携带主键值跳转到子页面',
                                'type' => 'openTab',
                                'url' => build_url('edit'),
                                'dropdown' => true
                            ],
                            'open_popup_page' => [
                                'title' => '携带主键值跳转到弹出层页面',
                                'type' => 'openPopup',
                                'url' => build_url('edit'),
                                'dropdown' => true
                            ]
                        ]
                    ],
                ];
            },
            "toolbar" => function (YunjTable $builder, $state) {
                return [
                    'back_get_pk' => [
                        'title' => '后端获取选定数据的主键',
                        'type' => 'asyncEvent'
                    ],
                    'front_get_pk' => [
                        'title' => '前端获取选定数据的主键'
                    ]
                ];
            },
            "defaultToolbar" => function (YunjTable $builder, $state) {
                return [
                    'filter', 'export',
                    'back_get_pk' => [
                        'title' => '后端获取选定数据的主键',
                        'class' => 'layui-icon-note',
                        'type' => 'asyncEvent'
                    ],
                    'front_get_pk' => [
                        'title' => '前端获取选定数据的主键',
                        'class' => 'layui-icon-note',
                    ]
                ];
            },
            "count" => function (YunjTable $builder, $filter) {
                return 3;
            },
            "items" => function (YunjTable $builder, $page, $pageSize, $filter, $sort) {
                return [
                    ['code' => 'aaa', 'name' => '产品1', 'attr' => '属性1'],
                    ['code' => 'bbb', 'name' => '产品2', 'attr' => '属性2'],
                    ['code' => 'ccc', 'name' => '产品3', 'attr' => '属性3'],
                ];
            },
            "event" => function (YunjTable $builder, $event, $codes) {
                switch ($event) {
                    case 'back_get_pk':
                        return response_json(10000, json_encode(['event' => $event, 'codes' => $codes]));
                    case TableBuilderEvent::SORT:
                        return success_json();
                }
                return error_json("演示数据不能进行操作");
            }
        ]);
        $builder->assign();
        return $this->view("table/index/pk");
    }

    public function pkCodeView() {
        return $this->view("table/index/pk_code_view");
    }

}