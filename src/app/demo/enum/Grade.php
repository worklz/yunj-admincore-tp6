<?php

namespace yunj\app\demo\enum;

final class Grade extends Enum {

    const ONE = 1;

    const TWO = 2;

    const THREE = 3;

    const FOUR = 4;

    public static function allEnumAttrs(): array {
        return [
            self::ONE=>[
                'title'=>'一年级',
            ],
            self::TWO=>[
                'title'=>'二年级',
            ],
            self::THREE=>[
                'title'=>'三年级',
            ],
            self::FOUR=>[
                'title'=>'四年级',
            ]
        ];
    }

}