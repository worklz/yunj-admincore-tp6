<?php

namespace yunj\app\demo\enum;

final class Hobby extends Enum {

    const WRITE = "write";

    const READ = "read";

    public static function allEnumAttrs(): array {
        return [
            self::WRITE=>[
                'title'=>'写作',
            ],
            self::READ=>[
                'title'=>'阅读',
            ]
        ];
    }

}