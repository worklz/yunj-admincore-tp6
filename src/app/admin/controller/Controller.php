<?php

namespace yunj\app\admin\controller;

use yunj\app\admin\traits\MemberHelper;
use yunj\app\admin\traits\ModelGet;
use yunj\core\Controller as YunjController;

abstract class Controller extends YunjController {

    use ModelGet,MemberHelper;

    /**
     * 模板输出
     * @param string $template
     * @param array $vars
     * @param int $code
     * @param null $filter
     * @return \think\response\View
     */
    protected function view(string $template = '', array $vars = [], int $code = 200, $filter = null) {
        $template = yunj_vender_app_admin_view_template($template);
        return view($template, $vars, $code, $filter);
    }

}