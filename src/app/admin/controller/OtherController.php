<?php

namespace yunj\app\admin\controller;

use yunj\core\enum\TipsTemplet;
use yunj\core\YunjFile;

class OtherController extends Controller {

    // 文件上传
    public function fileUpload($from = 'input') {
        return YunjFile::getInstance()->upload($from);
    }

    // 图片验证码
    public function captcha() {
        $data = yunj_captcha()->generateImageBase64();
        return success_json($data);
    }

    // 提示
    public function tips() {
        $param = input('get.') ?: [];
        $templet = isset($param['templet']) ? (int)$param['templet'] : null;
        /** @var TipsTemplet $templet */
        $templet = $templet && TipsTemplet::isValue($templet) ? TipsTemplet::get($templet) : TipsTemplet::ERROR();
        $data = [
            "code" => $templet->getValue(),
            "msg" => $param['msg'] ?? $templet->getMsg(),
            "action" => $templet->getAction()
        ];
        if ($data['code'] == 200) $data['msg'] .= '当前时间：' . date('Y-m-d H:i:s');
        return $this->view("tips", $data);
    }

    // 空
    public function _empty() {
        if (request()->isAjax()) {
            return success_json();
        }
        die();
    }

}