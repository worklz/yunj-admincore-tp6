<?php

namespace yunj\app\admin\controller\member;

use yunj\app\admin\controller\Controller;
use yunj\app\admin\enum\State;
use yunj\app\admin\service\member\MemberRoleService;

class MemberRoleController extends Controller {

    /**
     * @return MemberRoleService
     */
    private function getService(): MemberRoleService {
        return app(MemberRoleService::class);
    }

    public function lists() {
        $builder = $this->getService()->getListBuilder();
        $builder->assign();
        return view_table($builder);
    }

    public function add() {
        $builder = $this->getService()->getFormBuilder();
        $builder->assign();
        return view_form($builder);
    }

    public function edit() {
        $builder = $this->getService()->getFormBuilder(true);
        $builder->assign();
        return view_form($builder);
    }

}