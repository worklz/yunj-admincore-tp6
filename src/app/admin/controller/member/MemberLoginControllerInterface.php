<?php

namespace yunj\app\admin\controller\member;

use yunj\app\admin\model\AdminMember;

interface MemberLoginControllerInterface {

    /**
     * 登录
     * @return mixed
     */
    public function login();

    /**
     * 退出/切换账户
     * @return mixed
     */
    public function logout();

    /**
     * 生成token
     * @param AdminMember $member
     * @return string
     */
    public function generateToken(AdminMember $member): string;

    /**
     * 获取当前token
     * @return string
     */
    public function getRequestToken(): string;

}