<?php

namespace yunj\app\admin\controller\member;

use think\facade\Route;
use yunj\app\admin\enum\State;
use yunj\app\admin\controller\Controller;
use yunj\app\admin\service\member\log\LogQueryService;

class MemberLogController extends Controller {

    /**
     * @return LogQueryService
     */
    private function getService(): LogQueryService {
        return app(LogQueryService::class);
    }

    public function lists() {
        $builder = $this->getService()->getListBuilder();
        $builder->assign();
        return view_table($builder);
    }

}