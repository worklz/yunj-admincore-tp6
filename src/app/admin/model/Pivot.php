<?php

namespace yunj\app\admin\model;

use think\model\Pivot as ThinkPivot;
use yunj\core\traits\ModelHelper;

// 多对多中间表模型类
class Pivot extends ThinkPivot {

    use ModelHelper;
}
