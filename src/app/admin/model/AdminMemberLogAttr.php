<?php

namespace yunj\app\admin\model;

use yunj\core\traits\ModelSplit;

/**
 * 后台成员日志属性表，按月份分表
 */
class AdminMemberLogAttr extends Model {

    use ModelSplit;
}