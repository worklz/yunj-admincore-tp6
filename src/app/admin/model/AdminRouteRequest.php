<?php

namespace yunj\app\admin\model;

use think\db\BaseQuery;
use think\facade\Db;
use yunj\app\admin\enum\IsDemo;

class AdminRouteRequest extends Model {

    public function getJoinRouteRows(array $where = [], $field = ["*"], $order = [], int $page = 0, int $pageSize = 0) {
        $datas = Db::table($this->getTable())->alias('rr')
            ->join('admin_route r', 'r.id = rr.route_id')
            ->where($where)->field($field)->order($order);
        if ($page && $pageSize) $datas->limit(($page - 1) * $pageSize, $pageSize);
        $datas = $datas->select();
        return $datas;
    }

    public function getJoinRouteRowsToArray(array $where = [], $field = ["*"], $order = [], int $page = 0, int $pageSize = 0): array {
        $datas = $this->getJoinRouteRows($where, $field, $order, $page, $pageSize);
        return $datas ? $datas->toArray() : [];
    }

    public function getJoinRouteRow(array $where = [], $field = ["*"]) {
        $data = Db::table($this->getTable())->alias('rr')
            ->join('admin_route r', 'r.id = rr.route_id')
            ->where($where)->field($field)->find();
        return $data;
    }

    public function getJoinRouteRowToArray(array $where = [], $field = ["*"]): array {
        $data = $this->getJoinRouteRow($where, $field);
        if ($data && !is_array($data)) {
            $data = $data->toArray();
        }
        return $data ?: [];
    }

}