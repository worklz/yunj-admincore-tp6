<?php

namespace yunj\app\admin\model;

class AdminMemberRoleRelation extends Model {

    public function getJoinRoleRows(array $where = [], $field = ["*"], $order = [], int $page = 0, int $pageSize = 0) {
        $datas = Db::table($this->getTable())->alias('rr')
            ->join('admin_member_role r', 'r.id = rr.role_id')
            ->where($where)->field($field)->order($order);
        if ($page && $pageSize) $datas->limit(($page - 1) * $pageSize, $pageSize);
        $datas = $datas->select();
        return $datas;
    }

    public function getJoinRoleRowsToArray(array $where = [], $field = ["*"], $order = [], int $page = 0, int $pageSize = 0): array {
        $datas = $this->getJoinRoleRows($where, $field, $order, $page, $pageSize);
        return $datas ? $datas->toArray() : [];
    }

}