<?php
// +----------------------------------------------------------------------
// | 云静Admin
// +----------------------------------------------------------------------
// | Copyright (c) 2019-2022 http://www.iyunj.cn
// +----------------------------------------------------------------------
// | 云静Admin提供个人非商业用途免费使用。
// +----------------------------------------------------------------------
// | Author: Uncle-L <1071446619@qq.com>
// +----------------------------------------------------------------------
// | 应用的公共函数库
// +----------------------------------------------------------------------

use yunj\app\admin\service\auth\AuthService;
use yunj\app\admin\service\setting\SettingService;
use yunj\app\admin\service\member\MemberLoginService;
use yunj\app\admin\controller\member\MemberLoginController;
use yunj\app\admin\controller\member\MemberLoginControllerInterface;

if (!function_exists('admin_top_menu_html_layout')) {
    /**
     * 获取当前的顶部菜单html结构（最多支持两级）
     * @return string
     */
    function admin_top_menu_html_layout(): string {
        return AuthService::getInstance()->getCurrTopMenuHtmlLayout();
    }
}

if (!function_exists('admin_sidebar_menu_html_layout')) {
    /**
     * 获取当前的侧边栏菜单html结构
     * @return string
     */
    function admin_sidebar_menu_html_layout(): string {
        return AuthService::getInstance()->getCurrSidebarMenuHtmlLayout();
    }
}

if (!function_exists('admin_breadcrumb_html_layout')) {
    /**
     * 获取当前页面面包屑html结构
     * @return string
     */
    function admin_breadcrumb_html_layout(): string {
        return AuthService::getInstance()->getCurrBreadcrumbHtmlLayout();
    }
}

if (!function_exists('admin_global_search_result_html_layout')) {
    /**
     * 获取全局搜索所有结果的html结构
     * @return string
     */
    function admin_global_search_result_html_layout(): string {
        return AuthService::getInstance()->getCurrGlobalSearchResultHtmlLayout();
    }
}

if (!function_exists('admin_member_auth_check')) {
    /**
     * 校验当前管理员是否拥有指定权限
     * @param string|array|mixed $auth [权限|权限key]
     * @return string
     */
    function admin_member_auth_check($auth = ''): bool {
        return AuthService::getInstance()->check($auth);
    }
}

if (!function_exists('admin_member_login_service')) {
    /**
     * 后台成员登录服务
     * @return MemberLoginService
     */
    function admin_member_login_service(): MemberLoginService {
        return app(MemberLoginService::class);
    }
}

if (!function_exists('admin_member_login_controller')) {
    /**
     * 后台成员登录控制器
     * @return MemberLoginControllerInterface
     */
    function admin_member_login_controller(): MemberLoginControllerInterface {
        $controllerClass = yunj_config('admin.login_controller');
        $controllerClass = $controllerClass ?: MemberLoginController::class;
        if (!class_exists($controllerClass)) {
            throw new \RuntimeException('配置文件[admin.php]下[login_controller]配置错误');
        }
        $controller = app($controllerClass);
        if (!($controller instanceof MemberLoginControllerInterface)) {
            throw new \RuntimeException('配置文件[admin.php]下[login_controller]配置需实现接口"\yunj\app\admin\controller\member\MemberLoginControllerInterface"');
        }
        return $controller;
    }
}

if (!function_exists('admin_member_log')) {
    /**
     * 后台日志记录并返回日志数据
     * @param array $append_args 补充参数，如：['data_id'=>999,'desc'=>'描述']
     * @return array
     */
    function admin_member_log(array $append_args = []): array {
        return \yunj\app\admin\service\member\log\LogRecordService::handle($append_args);
    }
}