<?php
// +----------------------------------------------------------------------
// | 云静Admin
// +----------------------------------------------------------------------
// | Copyright (c) 2019-2022 http://www.iyunj.cn
// +----------------------------------------------------------------------
// | 云静Admin提供个人非商业用途免费使用。
// +----------------------------------------------------------------------
// | Author: Uncle-L <1071446619@qq.com>
// +----------------------------------------------------------------------
// | 成员模块权限配置
// +----------------------------------------------------------------------

/**
 * 权限项
 * 'key' => [
 *       'name' => '',                  // 描述
 *       'icon' => '',                  // 图标
 *       'parent' => '',                // 对应父级key
 *       'type' => AuthType,            // 类型
 *       'page_open' => AuthPageOpen,   // 页面打开方式
 *       'url' => ['', '', []],         // 地址对应 [需要判断权限时:基础地址不含域名和QUERY_STRING；仅为外部跳转链接时:全地址,请求方式method默认get,必须参数]
 *       'layout' => null,              // 重写权限html结构
 *   ],...
 */

use yunj\core\constants\BuilderConst;
use yunj\app\admin\enum\auth\AuthType;
use yunj\app\admin\enum\auth\AuthPageOpen;
use yunj\app\admin\enum\TableBuilderEvent;
use yunj\app\admin\service\setting\SettingService;
use yunj\app\admin\service\member\log\LogQueryService;

// 侧边栏后台
$sidebarAdminAuths = [
    // 管理员
    'yunj_admin_manage' => [
        'name' => '后台管理',
        'icon' => 'layui-icon-layouts',
        'parent' => 'yunj_system',
        'type' => AuthType::SIDEBAR_MENU
    ],
];

// 侧边栏管理员
$sidebarMemberMenuAuths = [
    // 管理员
    'yunj_member_manage' => [
        'name' => '管理员',
        'icon' => 'layui-icon-user',
        'parent' => 'yunj_admin_manage',
        'type' => AuthType::SIDEBAR_MENU,
    ],
    // 管理员列表
    'yunj_member_list' => [
        'name' => '管理员列表',
        'icon' => 'layui-icon-list',
        'parent' => 'yunj_member_manage',
        'type' => AuthType::SIDEBAR_MENU,
        'page_open' => AuthPageOpen::TAB,
        'url' => [build_url('yunjAdminMemberList'), 'get'],
    ],
    // 管理员列表查看
    'yunj_member_list_view' => [
        'name' => '查看',
        'parent' => 'yunj_member_list',
        'url' => [build_url('yunjAdminMemberList'), 'post'],
    ],
    'yunj_member_list_view_normal' => [
        'name' => '普通字段',
        'parent' => 'yunj_member_list_view',
    ],
    'yunj_member_list_view_last_login_ip' => [
        'parent' => 'yunj_member_list_view',
        'name' => '最后一次登录IP',
    ],
    'yunj_member_list_view_last_login_time' => [
        'parent' => 'yunj_member_list_view',
        'name' => '最后一次登录时间',
    ],
    'yunj_member_list_view_last_update_time' => [
        'parent' => 'yunj_member_list_view',
        'name' => '最后一次修改时间',
    ],
    // 管理员列表筛选
    'yunj_member_list_filter' => [
        'name' => '筛选',
        'parent' => 'yunj_member_list',
    ],
    'yunj_member_list_filter_keywords' => [
        'name' => '关键词 姓名/账户',
        'parent' => 'yunj_member_list_filter',
    ],
    'yunj_member_list_filter_role' => [
        'name' => '角色',
        'parent' => 'yunj_member_list_filter',
    ],
    'yunj_member_list_filter_create_time_range' => [
        'name' => '添加时间范围',
        'parent' => 'yunj_member_list_filter',
    ],
    // 管理员列表操作
    'yunj_member_list_action' => [
        'name' => '操作',
        'parent' => 'yunj_member_list',
    ],
    // 管理员新增
    'yunj_member_add' => [
        'name' => '新增',
        'parent' => 'yunj_member_list_action',
        'page_open' => AuthPageOpen::POPUP,
        'url' => [build_url('yunjAdminMemberAdd')],
    ],
    // 管理员编辑
    'yunj_member_edit' => [
        'name' => '编辑',
        'parent' => 'yunj_member_list_action',
    ],
    // 管理员编辑详情
    'yunj_member_edit_detail' => [
        'name' => '编辑详情',
        'parent' => 'yunj_member_edit',
        'url' => [build_url('yunjAdminMemberEdit'), 'get'],
        'page_open' => AuthPageOpen::POPUP,
    ],
    'yunj_member_edit_detail_normal' => [
        'name' => '普通字段',
        'parent' => 'yunj_member_edit_detail',
    ],
    'yunj_member_edit_detail_username' => [
        'name' => '账户',
        'parent' => 'yunj_member_edit_detail',
    ],
    'yunj_member_edit_detail_password' => [
        'name' => '密码',
        'parent' => 'yunj_member_edit_detail',
    ],
    'yunj_member_edit_detail_role_ids' => [
        'name' => '角色',
        'parent' => 'yunj_member_edit_detail',
    ],
    // 管理员编辑提交
    'yunj_member_edit_submit' => [
        'name' => '编辑提交',
        'parent' => 'yunj_member_edit',
        'url' => [build_url('yunjAdminMemberEdit'), 'post', [BuilderConst::ASYNC_TYPE_KEY => 'submit']]
    ],
    // 管理员移入回收站
    'yunj_member_recyle_bin' => [
        'name' => '移入回收站',
        'parent' => 'yunj_member_list_action',
        'url' => [build_url('yunjAdminMemberList'), 'post', [BuilderConst::ASYNC_TYPE_KEY => 'event', BuilderConst::ASYNC_EVENT_KEY => TableBuilderEvent::RECYLE_BIN]],
    ],
    // 管理员恢复正常
    'yunj_member_normal' => [
        'name' => '恢复正常',
        'parent' => 'yunj_member_list_action',
        'url' => [build_url('yunjAdminMemberList'), 'post', [BuilderConst::ASYNC_TYPE_KEY => 'event', BuilderConst::ASYNC_EVENT_KEY => TableBuilderEvent::NORMAL]],
    ],
    // 管理员删除
    'yunj_member_deleted' => [
        'name' => '删除',
        'parent' => 'yunj_member_list_action',
        'url' => [build_url('yunjAdminMemberList'), 'post', [BuilderConst::ASYNC_TYPE_KEY => 'event', BuilderConst::ASYNC_EVENT_KEY => TableBuilderEvent::DELETED]],
    ],
    // 管理员导出
    'yunj_member_export' => [
        'name' => '导出',
        'parent' => 'yunj_member_list_action',
        'url' => [build_url('yunjAdminMemberList'), 'post', [BuilderConst::ASYNC_TYPE_KEY => 'export']]
    ],
];

// 侧边栏角色
$sidebarRoleMenuAuths = [
    // 角色列表
    'yunj_role_list' => [
        'name' => '角色列表',
        'icon' => 'layui-icon-list',
        'parent' => 'yunj_member_manage',
        'type' => AuthType::SIDEBAR_MENU,
        'page_open' => AuthPageOpen::TAB,
        'url' => [build_url('yunjAdminMemberRoleList'), 'get'],
    ],
    'yunj_role_list_view' => [
        'name' => '查看',
        'parent' => 'yunj_role_list',
        'url' => [build_url('yunjAdminMemberRoleList'), 'post'],
    ],
    'yunj_role_list_view_normal' => [
        'name' => '普通字段',
        'parent' => 'yunj_role_list_view',
    ],
    // 角色列表操作
    'yunj_role_list_action' => [
        'name' => '操作',
        'parent' => 'yunj_role_list',
    ],
    // 角色新增
    'yunj_role_add' => [
        'name' => '新增',
        'parent' => 'yunj_role_list_action',
        'url' => [build_url('yunjAdminMemberRoleAdd')],
        'page_open' => AuthPageOpen::TAB,
    ],
    // 角色编辑
    'yunj_role_edit' => [
        'name' => '编辑详情',
        'parent' => 'yunj_role_list_action',
    ],
    // 角色编辑详情
    'yunj_role_edit_detail' => [
        'name' => '编辑详情',
        'parent' => 'yunj_role_edit',
        'url' => [build_url('yunjAdminMemberRoleEdit'), 'get'],
        'page_open' => AuthPageOpen::TAB,
    ],
    'yunj_role_edit_detail_normal' => [
        'name' => '普通字段',
        'parent' => 'yunj_role_edit_detail',
    ],
    'yunj_role_edit_detail_alias' => [
        'name' => '别名',
        'parent' => 'yunj_role_edit_detail',
    ],
    'yunj_role_edit_detail_auths' => [
        'name' => '权限配置',
        'parent' => 'yunj_role_edit_detail',
    ],
    // 角色编辑提交
    'yunj_role_edit_submit' => [
        'name' => '编辑提交',
        'parent' => 'yunj_role_edit',
        'url' => [build_url('yunjAdminMemberRoleEdit'), 'post', [BuilderConst::ASYNC_TYPE_KEY => 'submit']]
    ],
    // 角色移入回收站
    'yunj_role_recyle_bin' => [
        'name' => '移入回收站',
        'parent' => 'yunj_role_list_action',
        'url' => [build_url('yunjAdminMemberRoleList'), 'post', [BuilderConst::ASYNC_TYPE_KEY => 'event', BuilderConst::ASYNC_EVENT_KEY => TableBuilderEvent::RECYLE_BIN]],
    ],
    // 角色恢复正常
    'yunj_role_normal' => [
        'name' => '恢复正常',
        'parent' => 'yunj_role_list_action',
        'url' => [build_url('yunjAdminMemberRoleList'), 'post', [BuilderConst::ASYNC_TYPE_KEY => 'event', BuilderConst::ASYNC_EVENT_KEY => TableBuilderEvent::NORMAL]],
    ],
    // 角色删除
    'yunj_role_deleted' => [
        'name' => '删除',
        'parent' => 'yunj_role_list_action',
        'url' => [build_url('yunjAdminMemberRoleList'), 'post', [BuilderConst::ASYNC_TYPE_KEY => 'event', BuilderConst::ASYNC_EVENT_KEY => TableBuilderEvent::DELETED]],
    ],
    // 角色排序
    'yunj_role_sort' => [
        'name' => '排序',
        'parent' => 'yunj_role_list_action',
        'url' => [build_url('yunjAdminMemberRoleList'), 'post', [BuilderConst::ASYNC_TYPE_KEY => 'event', BuilderConst::ASYNC_EVENT_KEY => TableBuilderEvent::SORT]],
    ],
    // 角色导出
    'yunj_role_export' => [
        'name' => '导出',
        'parent' => 'yunj_role_list_action',
        'url' => [build_url('yunjAdminMemberRoleList'), 'post', [BuilderConst::ASYNC_TYPE_KEY => 'export']]
    ],
];

// 侧边栏管理员日志
$memberLogAuths = LogQueryService::auths();

// 侧边栏路由管理
$sidebarRouteMenuAuths = [
    // 路由管理
    'yunj_route_list' => [
        'name' => '路由管理',
        'icon' => 'layui-icon-list',
        'parent' => 'yunj_admin_manage',
        'type' => AuthType::SIDEBAR_MENU,
        'page_open' => AuthPageOpen::TAB,
        'url' => [build_url('yunjAdminRouteList'), 'get'],
    ],
    // 路由查看
    'yunj_route_list_view' => [
        'name' => '查看',
        'parent' => 'yunj_route_list',
        'url' => [build_url('yunjAdminRouteList'), 'post'],
    ],
    'yunj_route_list_view_normal' => [
        'name' => '普通字段',
        'parent' => 'yunj_route_list_view',
    ],
    // 路由筛选
    'yunj_route_list_filter' => [
        'name' => '筛选',
        'parent' => 'yunj_route_list',
    ],
    'yunj_route_list_filter_keywords' => [
        'name' => '关键词',
        'parent' => 'yunj_route_list_filter',
    ],
    // 路由操作
    'yunj_route_list_action' => [
        'name' => '操作',
        'parent' => 'yunj_route_list',
    ],
    // 路由新增
    'yunj_route_add' => [
        'name' => '新增',
        'parent' => 'yunj_route_list_action',
        'page_open' => AuthPageOpen::POPUP,
        'url' => [build_url('yunjAdminRouteAdd')],
    ],
    // 路由编辑
    'yunj_route_edit' => [
        'name' => '编辑',
        'parent' => 'yunj_route_list_action',
    ],
    // 路由编辑详情
    'yunj_route_edit_detail' => [
        'name' => '编辑详情',
        'parent' => 'yunj_route_edit',
        'url' => [build_url('yunjAdminRouteEdit'), 'get'],
        'page_open' => AuthPageOpen::POPUP,
    ],
    'yunj_route_edit_detail_normal' => [
        'name' => '普通字段',
        'parent' => 'yunj_route_edit_detail',
    ],
    // 路由编辑提交
    'yunj_route_edit_submit' => [
        'name' => '编辑提交',
        'parent' => 'yunj_route_edit',
        'url' => [build_url('yunjAdminRouteEdit'), 'post']
    ],
    // 路由排序
    'yunj_route_sort' => [
        'name' => '排序',
        'parent' => 'yunj_route_list_action',
        'url' => [build_url('yunjAdminRouteSort')],
    ],
    // 路由移入回收站
    'yunj_route_recyle_bin' => [
        'name' => '移入回收站',
        'parent' => 'yunj_route_list_action',
        'url' => [build_url('yunjAdminRouteList'), 'post', [BuilderConst::ASYNC_TYPE_KEY => 'event', BuilderConst::ASYNC_EVENT_KEY => TableBuilderEvent::RECYLE_BIN]],
    ],
    // 路由恢复正常
    'yunj_route_normal' => [
        'name' => '恢复正常',
        'parent' => 'yunj_route_list_action',
        'url' => [build_url('yunjAdminRouteList'), 'post', [BuilderConst::ASYNC_TYPE_KEY => 'event', BuilderConst::ASYNC_EVENT_KEY => TableBuilderEvent::NORMAL]],
    ],
    // 路由删除
    'yunj_route_deleted' => [
        'name' => '删除',
        'parent' => 'yunj_route_list_action',
        'url' => [build_url('yunjAdminRouteList'), 'post', [BuilderConst::ASYNC_TYPE_KEY => 'event', BuilderConst::ASYNC_EVENT_KEY => TableBuilderEvent::DELETED]],
    ],
    // 路由导出
    'yunj_route_export' => [
        'name' => '导出',
        'parent' => 'yunj_route_list_action',
        'url' => [build_url('yunjAdminRouteList'), 'post', [BuilderConst::ASYNC_TYPE_KEY => 'export']]
    ],
    // 同步系统路由数据
    'yunj_route_sync_system_datas' => [
        'name' => '同步系统路由数据',
        'parent' => 'yunj_route_list_action',
        'url' => [build_url('yunjAdminRouteList'), 'post', [BuilderConst::ASYNC_TYPE_KEY => 'syncSystemDatas']]
    ],
    // 清理缓存
    'yunj_route_clear_cache' => [
        'name' => '清理缓存',
        'parent' => 'yunj_route_list_action',
        'url' => [build_url('yunjAdminRouteList'), 'post', [BuilderConst::ASYNC_TYPE_KEY => 'clearCache']]
    ],
];

// 侧边栏权限管理
$_sidebarAuthMenuAuths = [
    // 权限管理
    'yunj_auth_list' => [
        'name' => '权限管理',
        'icon' => 'layui-icon-list',
        'parent' => 'yunj_admin_manage',
        'type' => AuthType::SIDEBAR_MENU,
        'page_open' => AuthPageOpen::TAB,
        'url' => [build_url('yunjAdminAuthList'), 'get'],
    ],
    // 权限管理查看
    'yunj_auth_list_view' => [
        'name' => '查看',
        'parent' => 'yunj_auth_list',
        'url' => [build_url('yunjAdminAuthList'), 'post'],
    ],
    'yunj_auth_list_view_normal' => [
        'name' => '普通字段',
        'parent' => 'yunj_auth_list_view',
    ],
    // 权限管理筛选
    'yunj_auth_list_filter' => [
        'name' => '筛选',
        'parent' => 'yunj_auth_list',
    ],
    'yunj_auth_list_filter_key' => [
        'name' => '权限key',
        'parent' => 'yunj_auth_list_filter',
    ],
    'yunj_auth_list_filter_keywords' => [
        'name' => '关键词',
        'parent' => 'yunj_auth_list_filter',
    ],
    // 权限管理操作
    'yunj_auth_list_action' => [
        'name' => '操作',
        'parent' => 'yunj_auth_list',
    ],
    // 权限管理新增
    'yunj_auth_add' => [
        'name' => '新增',
        'parent' => 'yunj_auth_list_action',
        'page_open' => AuthPageOpen::POPUP,
        'url' => [build_url('yunjAdminAuthAdd')],
    ],
    // 权限管理编辑
    'yunj_auth_edit' => [
        'name' => '编辑',
        'parent' => 'yunj_auth_list_action',
    ],
    // 权限管理编辑详情
    'yunj_auth_edit_detail' => [
        'name' => '编辑详情',
        'parent' => 'yunj_auth_edit',
        'url' => [build_url('yunjAdminAuthEdit'), 'get'],
        'page_open' => AuthPageOpen::POPUP,
    ],
    'yunj_auth_edit_detail_normal' => [
        'name' => '普通字段',
        'parent' => 'yunj_auth_edit_detail',
    ],
    // 权限管理编辑提交
    'yunj_auth_edit_submit' => [
        'name' => '编辑提交',
        'parent' => 'yunj_auth_edit',
        'url' => [build_url('yunjAdminAuthEdit'), 'post']
    ],
    // 权限排序
    'yunj_auth_sort' => [
        'name' => '权限排序',
        'parent' => 'yunj_auth_list_action',
        'url' => [build_url('yunjAdminAuthSort')],
    ],
    // 权限管理移入回收站
    'yunj_auth_recyle_bin' => [
        'name' => '移入回收站',
        'parent' => 'yunj_auth_list_action',
        'url' => [build_url('yunjAdminAuthList'), 'post', [BuilderConst::ASYNC_TYPE_KEY => 'event', BuilderConst::ASYNC_EVENT_KEY => TableBuilderEvent::RECYLE_BIN]],
    ],
    // 权限管理恢复正常
    'yunj_auth_normal' => [
        'name' => '恢复正常',
        'parent' => 'yunj_auth_list_action',
        'url' => [build_url('yunjAdminAuthList'), 'post', [BuilderConst::ASYNC_TYPE_KEY => 'event', BuilderConst::ASYNC_EVENT_KEY => TableBuilderEvent::NORMAL]],
    ],
    // 权限管理删除
    'yunj_auth_deleted' => [
        'name' => '删除',
        'parent' => 'yunj_auth_list_action',
        'url' => [build_url('yunjAdminAuthList'), 'post', [BuilderConst::ASYNC_TYPE_KEY => 'event', BuilderConst::ASYNC_EVENT_KEY => TableBuilderEvent::DELETED]],
    ],
    // 权限管理导出
    'yunj_auth_export' => [
        'name' => '导出',
        'parent' => 'yunj_auth_list_action',
        'url' => [build_url('yunjAdminAuthList'), 'post', [BuilderConst::ASYNC_TYPE_KEY => 'export']]
    ],
    // 同步系统权限
    'yunj_auth_sync_system_auths' => [
        'name' => '同步系统权限管理',
        'parent' => 'yunj_auth_list_action',
        'url' => [build_url('yunjAdminAuthList'), 'post', [BuilderConst::ASYNC_TYPE_KEY => 'syncSystemDatas']]
    ],
    // 清理缓存
    'yunj_auth_clear_cache' => [
        'name' => '清理缓存',
        'parent' => 'yunj_auth_list_action',
        'url' => [build_url('yunjAdminAuthList'), 'post', [BuilderConst::ASYNC_TYPE_KEY => 'clearCache']]
    ],
];

$sidebarAdminAuths = $sidebarAdminAuths
    + $sidebarMemberMenuAuths
    + $sidebarRoleMenuAuths
    + $memberLogAuths
    + $sidebarRouteMenuAuths
    + $_sidebarAuthMenuAuths;

// 侧边栏设置
$settingMenuAuths = SettingService::auths();

// 侧边栏菜单权限
$sidebarMenuAuths = [
        // 系统
        'yunj_system' => [
            'name' => '系统',
            'icon' => 'layui-icon-engine',
            'type' => AuthType::SIDEBAR_MENU,
        ],
    ] + $settingMenuAuths + $sidebarAdminAuths;

// 其他权限
$otherAuths = [
    'yunj_top_right_action' => [
        'name' => '页面右上角操作'
    ],
    'yunj_top_right_action_global_search' => [
        'name' => '全局搜索',
        'parent' => 'yunj_top_right_action'
    ],
];

return $sidebarMenuAuths + $otherAuths;