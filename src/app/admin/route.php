<?php
// +----------------------------------------------------------------------
// | 云静Admin
// +----------------------------------------------------------------------
// | Copyright (c) 2019-2022 http://www.iyunj.cn
// +----------------------------------------------------------------------
// | 云静Admin提供个人非商业用途免费使用。
// +----------------------------------------------------------------------
// | Author: Uncle-L <1071446619@qq.com>
// +----------------------------------------------------------------------
// | 请求配置
// +----------------------------------------------------------------------

use yunj\app\admin\enum\EnableLoginAuth;
use yunj\core\constants\BuilderConst;
use yunj\app\admin\enum\TableBuilderEvent;
use yunj\app\admin\enum\route\RequestMethod;
use yunj\app\admin\service\setting\SettingService;
use yunj\app\admin\service\member\log\LogQueryService;
use yunj\app\admin\controller\IndexController;
use yunj\app\admin\controller\OtherController;
use yunj\app\admin\controller\auth\AuthController;
use yunj\app\admin\controller\member\MemberController;
use yunj\app\admin\controller\member\MemberRoleController;
use yunj\app\admin\controller\member\MemberLogController;
use yunj\app\admin\controller\setting\SettingController;
use yunj\app\admin\controller\route\RouteController;

$requests = [
    [
        'system_key' => '70929ec0de7cd3e92816f282bd32ca94',
        'type' => 'group',
        'name' => YUNJ_ADMIN_ENTRANCE,
        'namespace' => '',
        'desc' => '后台',
        'enable_login_auth' => EnableLoginAuth::ON,
        'items' => [
            [
                'system_key' => '701980682688f7f704e3b4a39838921b',
                'type' => 'route',
                'name' => 'yunjAdminIndex',
                'rule' => '/',
                'route' => class_route([IndexController::class, 'index']),
                'desc' => '首页',
                'items' => [
                    [
                        'system_key' => '263619801b1b9b0870e1253544aa64df',
                        'type' => 'request',
                        'method' => RequestMethod::GET,
                        'desc' => '首页'
                    ]
                ]
            ],
            [
                'system_key' => '0dc6752f175d7df1e01eeeeda77fd768',
                'type' => 'route',
                'name' => 'yunjAdminMemberLogout',
                'rule' => 'logout',
                'route' => class_route([get_class(admin_member_login_controller()), 'logout']),
                'desc' => '登出',
                'items' => [
                    [
                        'system_key' => 'f0fe51a305df76222636a2625f3d68a8',
                        'type' => 'request',
                        'desc' => '登出'
                    ]
                ]
            ],
            [
                'system_key' => 'bba6226e6f7e3a1d2fb50f9e3cc5150d',
                'type' => 'route',
                'name' => 'yunjAdminFileUpload',
                'rule' => 'file/upload',
                'route' => class_route([OtherController::class, 'fileUpload']),
                'desc' => '文件上传',
                'items' => [
                    [
                        'system_key' => '9f5d3b2acf9c1dfc6399e5f152be58ee',
                        'type' => 'request',
                        'desc' => '文件上传'
                    ]
                ]
            ],
            [
                'system_key' => '68f1169c7fcef3cb128c6d6eef72be3c',
                'type' => 'route',
                'name' => 'yunjSetting',
                'rule' => 'setting',
                'route' => class_route([SettingController::class, 'index']),
                'desc' => '系统设置',
                'items' => SettingService::requestItems()
            ],
            [
                'system_key' => '7a1320240669617f506fbae362b2375c',
                'type' => 'group',
                'name' => 'member',
                'namespace' => '',
                'desc' => '成员管理',
                'items' => [
                    [
                        'system_key' => '0f04b375ed56b262d61bb09dff42bbd6',
                        'type' => 'route',
                        'name' => 'yunjAdminMemberList',
                        'rule' => 'list',
                        'route' => class_route([MemberController::class, 'lists']),
                        'desc' => '成员列表',
                        'items' => [
                            [
                                'system_key' => 'f5e94e2d0578140d00e28f5f15ed27ce',
                                'type' => 'request',
                                'method' => RequestMethod::GET,
                                'desc' => '页面'
                            ],
                            [
                                'system_key' => 'ad2a7b90a0799af1d881f0bed5e51058',
                                'type' => 'request',
                                'method' => RequestMethod::POST,
                                'desc' => '数据获取'
                            ],
                            [
                                'system_key' => 'b7b404d48df2e17b5429042f31d4b11b',
                                'type' => 'request',
                                'method' => RequestMethod::POST,
                                'require_params' => [BuilderConst::ASYNC_TYPE_KEY => 'export'],
                                'desc' => '数据导出'
                            ],
                            [
                                'system_key' => '22ab72c5fec3058f3c786874cbdb30f0',
                                'type' => 'request',
                                'method' => RequestMethod::POST,
                                'require_params' => [
                                    BuilderConst::ASYNC_TYPE_KEY => 'event',
                                    BuilderConst::ASYNC_EVENT_KEY => TableBuilderEvent::RECYLE_BIN,
                                ],
                                'desc' => '数据移入回收站',
                            ],
                            [
                                'system_key' => 'da95f43a08f0978f1a27997c913107b4',
                                'type' => 'request',
                                'desc' => '数据恢复正常',
                                'method' => RequestMethod::POST,
                                'require_params' => [
                                    BuilderConst::ASYNC_TYPE_KEY => 'event',
                                    BuilderConst::ASYNC_EVENT_KEY => TableBuilderEvent::NORMAL,
                                ],
                            ],
                            [
                                'system_key' => 'e16440c421176acbbec29775f2a5743b',
                                'type' => 'request',
                                'desc' => '数据删除',
                                'method' => RequestMethod::POST,
                                'require_params' => [
                                    BuilderConst::ASYNC_TYPE_KEY => 'event',
                                    BuilderConst::ASYNC_EVENT_KEY => TableBuilderEvent::DELETED,
                                ],
                            ]
                        ]
                    ],
                    [
                        'system_key' => '4b242657f1cae5447fa3f953d578148a',
                        'type' => 'route',
                        'name' => 'yunjAdminMemberAdd',
                        'rule' => 'add',
                        'route' => class_route([MemberController::class, 'add']),
                        'desc' => '成员添加',
                        'items' => [
                            [
                                'system_key' => '3812cae22e5c27dc13f596de9b70ebc6',
                                'type' => 'request',
                                'desc' => '成员添加',
                            ]
                        ]
                    ],
                    [
                        'system_key' => '1cfa1872acdb81b2b5a9ec60ceffbfe3',
                        'type' => 'route',
                        'name' => 'yunjAdminMemberEdit',
                        'rule' => 'edit',
                        'route' => class_route([MemberController::class, 'edit']),
                        'desc' => '成员编辑',
                        'items' => [
                            [
                                'system_key' => '056a59ed1aa6a414d6874aed50a1861c',
                                'type' => 'request',
                                'desc' => '页面',
                                'method' => RequestMethod::GET,
                            ],
                            [
                                'system_key' => '9b57c675963176719ea667cafcbb887d',
                                'type' => 'request',
                                'desc' => '编辑数据保存',
                                'method' => RequestMethod::POST,
                            ]
                        ]
                    ],
                    [
                        'system_key' => '520b4487b6eebce694a58b3c2b9b4368',
                        'type' => 'route',
                        'name' => 'yunjAdminMemberProfile',
                        'rule' => 'profile',
                        'route' => class_route([MemberController::class, 'profile']),
                        'desc' => '成员资料',
                        'items' => [
                            [
                                'system_key' => '2cd2804232d62c005ec7a3137d38082c',
                                'type' => 'request',
                                'desc' => '页面',
                                'method' => RequestMethod::GET,
                            ],
                            [
                                'system_key' => 'ec5ef81bc90855ae0bf8724319d1250b',
                                'type' => 'request',
                                'desc' => '数据保存',
                                'method' => RequestMethod::POST,
                            ],
                        ]
                    ],
                    [
                        'system_key' => '0c505aaa3d19e1f91d007c982bec459c',
                        'type' => 'route',
                        'name' => 'yunjAdminMemberDropdownsearch',
                        'rule' => 'dropdownsearch',
                        'route' => class_route([MemberController::class, 'dropdownsearch']),
                        'desc' => '成员下拉搜索',
                        'items' => [
                            [
                                'system_key' => 'd006268615e42c882e1fa014f4a68fee',
                                'type' => 'request',
                                'desc' => '成员下拉搜索',
                            ],
                        ]
                    ],
                ]
            ],
            [
                'system_key' => '0808ecd0a71860aab42c54e310b5af12',
                'type' => 'group',
                'name' => 'member-role',
                'namespace' => '',
                'desc' => '角色管理',
                'items' => [
                    [
                        'system_key' => '3d803fb09295fcffb4afa98612a52b9d',
                        'type' => 'route',
                        'name' => 'yunjAdminMemberRoleList',
                        'rule' => 'list',
                        'route' => class_route([MemberRoleController::class, 'lists']),
                        'desc' => '角色列表',
                        'items' => [
                            [
                                'system_key' => '81d9199bbe63b0bd6be7c62bc156f6b9',
                                'type' => 'request',
                                'desc' => '页面',
                                'method' => RequestMethod::GET,
                            ],
                            [
                                'system_key' => 'a4c87a4e752bb50b33bcf52bf56ba777',
                                'type' => 'request',
                                'desc' => '数据获取',
                                'method' => RequestMethod::POST,
                            ],
                            [
                                'system_key' => '5927dd3242b397fdfb331fcb54fe9116',
                                'type' => 'request',
                                'desc' => '数据导出',
                                'method' => RequestMethod::POST,
                                'require_params' => [BuilderConst::ASYNC_TYPE_KEY => 'export'],
                            ],
                            [
                                'system_key' => '3180bb21ce0f57a528ff06ba88d13b3a',
                                'type' => 'request',
                                'desc' => '数据移入回收站',
                                'method' => RequestMethod::POST,
                                'require_params' => [
                                    BuilderConst::ASYNC_TYPE_KEY => 'event',
                                    BuilderConst::ASYNC_EVENT_KEY => TableBuilderEvent::RECYLE_BIN,
                                ],
                            ],
                            [
                                'system_key' => '5e73196c8295297a888c8715d574470b',
                                'type' => 'request',
                                'desc' => '数据恢复正常',
                                'method' => RequestMethod::POST,
                                'require_params' => [
                                    BuilderConst::ASYNC_TYPE_KEY => 'event',
                                    BuilderConst::ASYNC_EVENT_KEY => TableBuilderEvent::NORMAL,
                                ],
                            ],
                            [
                                'system_key' => '66c54ac4f52d102c4a641b29d2a9adcb',
                                'type' => 'request',
                                'desc' => '数据删除',
                                'method' => RequestMethod::POST,
                                'require_params' => [
                                    BuilderConst::ASYNC_TYPE_KEY => 'event',
                                    BuilderConst::ASYNC_EVENT_KEY => TableBuilderEvent::DELETED,
                                ],
                            ],
                            [
                                'system_key' => 'c9143f5e129dceb98ada87f33ebccf21',
                                'type' => 'request',
                                'desc' => '数据排序',
                                'method' => RequestMethod::POST,
                                'require_params' => [
                                    BuilderConst::ASYNC_TYPE_KEY => 'event',
                                    BuilderConst::ASYNC_EVENT_KEY => TableBuilderEvent::SORT,
                                ],
                            ],
                        ],
                    ],
                    [
                        'system_key' => 'af8bdbf6159a8574fb847d5215031fc1',
                        'type' => 'route',
                        'name' => 'yunjAdminMemberRoleAdd',
                        'rule' => 'add',
                        'route' => class_route([MemberRoleController::class, 'add']),

                        'desc' => '角色新增',
                        'items' => [
                            [
                                'system_key' => '41013d72950de320af61795684bc5bef',
                                'type' => 'request',
                                'desc' => '角色新增',
                            ],
                        ],
                    ],
                    [
                        'system_key' => '0afe20411453cb9566e284ce4fdbe65c',
                        'type' => 'route',
                        'name' => 'yunjAdminMemberRoleEdit',
                        'rule' => 'edit',
                        'route' => class_route([MemberRoleController::class, 'edit']),

                        'desc' => '角色编辑',
                        'items' => [
                            [
                                'system_key' => '7c4cf7ff4c2458e09084c63e5a239546',
                                'type' => 'request',
                                'desc' => '页面',
                                'method' => RequestMethod::GET,
                            ],
                            [
                                'system_key' => 'd8c58f9986c762d801766ff789881c89',
                                'type' => 'request',
                                'desc' => '数据保存',
                                'method' => RequestMethod::POST,
                            ],
                        ],
                    ]
                ]
            ],
            [
                'system_key' => 'be48a8d3ec8ef1c0c512db043a1b719d',
                'type' => 'group',
                'name' => 'member-log',
                'namespace' => '',
                'desc' => '日志管理',
                'items' => [
                    [
                        'system_key' => '5c2943d831930858ed5e3931ab0123da',
                        'type' => 'route',
                        'name' => 'yunjMemberLog',
                        'rule' => 'list',
                        'route' => class_route([MemberLogController::class, 'lists']),
                        'desc' => '日志列表',
                        'items' => LogQueryService::requestItems()
                    ]
                ]
            ],
            [
                'system_key' => '842e71e02c61d89dbe4c6477b2f9e94e',
                'type' => 'group',
                'name' => 'route',
                'namespace' => '',
                'desc' => '路由管理',
                'items' => [
                    [
                        'system_key' => '1bda94ec9469cf0c191c543d0e8353af',
                        'type' => 'route',
                        'name' => 'yunjAdminRouteList',
                        'rule' => 'list',
                        'route' => class_route([RouteController::class, 'lists']),
                        'desc' => '路由列表',
                        'items' => [
                            [
                                'system_key' => '4f698fc3ad87bdd573c95edd88d6054b',
                                'type' => 'request',
                                'desc' => '页面',
                                'method' => RequestMethod::GET,
                            ],
                            [
                                'system_key' => '0e8cc955001c009000ef01635391943c',
                                'type' => 'request',
                                'desc' => '常规数据查询/处理',
                                'method' => RequestMethod::POST,
                            ],
                            [
                                'system_key' => '49978809161e9fe2dbf339b9105428fd',
                                'type' => 'request',
                                'desc' => '移入回收站',
                                'method' => RequestMethod::POST,
                                'require_params' => [
                                    BuilderConst::ASYNC_TYPE_KEY => 'event',
                                    BuilderConst::ASYNC_EVENT_KEY => TableBuilderEvent::RECYLE_BIN
                                ]
                            ],
                            [
                                'system_key' => '7d481745e4cda5da52c6b21b0dd546a4',
                                'type' => 'request',
                                'desc' => '数据恢复正常',
                                'method' => RequestMethod::POST,
                                'require_params' => [
                                    BuilderConst::ASYNC_TYPE_KEY => 'event',
                                    BuilderConst::ASYNC_EVENT_KEY => TableBuilderEvent::NORMAL
                                ]
                            ],
                            [
                                'system_key' => 'b2149bb96d82dc59dba776b0904482e3',
                                'type' => 'request',
                                'desc' => '数据删除',
                                'method' => RequestMethod::POST,
                                'require_params' => [
                                    BuilderConst::ASYNC_TYPE_KEY => 'event',
                                    BuilderConst::ASYNC_EVENT_KEY => TableBuilderEvent::DELETED
                                ]
                            ],
                            [
                                'system_key' => 'ad3b872e743120f8d97e7ea7674026b4',
                                'type' => 'request',
                                'desc' => '数据导出',
                                'method' => RequestMethod::POST,
                                'require_params' => [
                                    BuilderConst::ASYNC_TYPE_KEY => 'export',
                                ]
                            ],
                            [
                                'system_key' => 'f3a94e3fead437e2cbc7cbfbdb4f38fd',
                                'type' => 'request',
                                'desc' => '同步系统路由数据',
                                'method' => RequestMethod::POST,
                                'require_params' => [
                                    BuilderConst::ASYNC_TYPE_KEY => 'syncSystemDatas',
                                ]
                            ],
                            [
                                'system_key' => '47dc9bc39c54bcb79ea4cb058b5562a1',
                                'type' => 'request',
                                'desc' => '清理缓存',
                                'method' => RequestMethod::POST,
                                'require_params' => [
                                    BuilderConst::ASYNC_TYPE_KEY => 'clearCache',
                                ]
                            ],
                        ],
                    ],
                    [
                        'system_key' => '74bc158d18ffe9bf6eb7538eb5cef520',
                        'type' => 'route',
                        'name' => 'yunjAdminRouteAdd',
                        'rule' => 'add',
                        'route' => class_route([RouteController::class, 'add']),
                        'desc' => '路由新增',
                        'items' => [
                            [
                                'system_key' => '9a2cc56d240ece69b1238022c790a503',
                                'type' => 'request',
                                'desc' => '请求新增',
                            ],
                        ],
                    ],
                    [
                        'system_key' => '561d8c487eac870eeaaae11959990193',
                        'type' => 'route',
                        'name' => 'yunjAdminRouteEdit',
                        'rule' => 'edit',
                        'route' => class_route([RouteController::class, 'edit']),
                        'desc' => '路由编辑',
                        'items' => [
                            [
                                'system_key' => '1e57f9f8cfa44673c12c44ad6bbc8c9c',
                                'type' => 'request',
                                'desc' => '页面',
                                'method' => RequestMethod::GET,
                            ],
                            [
                                'system_key' => '71750e5bf9c9ba02b0e7423de51ba67e',
                                'type' => 'request',
                                'desc' => '数据编辑提交',
                                'method' => RequestMethod::POST,
                            ],
                        ],
                    ],
                    [
                        'system_key' => 'a76e31876081dfd79bd785afbbbd6c9a',
                        'type' => 'route',
                        'name' => 'yunjAdminRouteSort',
                        'rule' => 'sort',
                        'route' => class_route([RouteController::class, 'sort']),
                        'desc' => '路由排序',
                        'items' => [
                            [
                                'system_key' => 'dd5a417533035c7fd482add3a8d5dc21',
                                'type' => 'request',
                                'desc' => '路由排序',
                            ],
                        ],
                    ],
                    [
                        'system_key' => 'bcca261826a10292b3d47c68a5f58698',
                        'type' => 'route',
                        'name' => 'yunjAdminRouteDropdownSearchGroupOptions',
                        'rule' => 'form-dropdown-search-group-options',
                        'route' => class_route([RouteController::class, 'dropdownSearchGroupOptions']),
                        'desc' => '路由分组下拉搜索',
                        'items' => __handle_builder_requests([
                            [
                                'system_key' => 'd7643eef96fa73172392414bb5c3c720',
                                'type' => 'request',
                                'desc' => '路由分组下拉搜索',
                            ]
                        ]),
                    ],
                    [
                        'system_key' => '5a3f75ed3e710eb7150b001b7e1154e9',
                        'type' => 'route',
                        'name' => 'yunjAdminRouteDropdownSearchRouteOptions',
                        'rule' => 'form-dropdown-search-route-options',
                        'route' => class_route([RouteController::class, 'dropdownSearchRouteOptions']),
                        'desc' => '路由下拉搜索',
                        'items' => __handle_builder_requests([
                            [
                                'system_key' => 'aafce2bb11601f81f59d434446d75be1',
                                'type' => 'request',
                                'desc' => '路由下拉搜索',
                            ]
                        ]),
                    ],
                ]
            ],
            [
                'system_key' => '2ec750fe2e0ec25317e8468aa37c3db1',
                'type' => 'group',
                'name' => 'auth',
                'namespace' => '',
                'desc' => '权限管理',
                'items' => [
                    [
                        'system_key' => '6ecffaee4a0ba0655555435ae8b9a8d1',
                        'type' => 'route',
                        'name' => 'yunjAdminAuthList',
                        'rule' => 'list',
                        'route' => class_route([AuthController::class, 'lists']),
                        'desc' => '权限列表',
                        'items' => [
                            [
                                'system_key' => '91c5c8f643709fcd862b1f9978ad8e81',
                                'type' => 'request',
                                'desc' => '页面',
                                'method' => RequestMethod::GET,
                            ],
                            [
                                'system_key' => '648f816182f52418a5a7be7f64a35900',
                                'type' => 'request',
                                'desc' => '常规数据查询/处理',
                                'method' => RequestMethod::POST,
                            ],
                            [
                                'system_key' => '6946c102ac06ecc0f0806dfa4f6aeaa1',
                                'type' => 'request',
                                'desc' => '移入回收站',
                                'method' => RequestMethod::POST,
                                'require_params' => [BuilderConst::ASYNC_TYPE_KEY => 'event', BuilderConst::ASYNC_EVENT_KEY => TableBuilderEvent::RECYLE_BIN]
                            ],
                            [
                                'system_key' => '998a2546dc30104a3998f6b4471fcb11',
                                'type' => 'request',
                                'desc' => '恢复正常',
                                'method' => RequestMethod::POST,
                                'require_params' => [BuilderConst::ASYNC_TYPE_KEY => 'event', BuilderConst::ASYNC_EVENT_KEY => TableBuilderEvent::NORMAL]
                            ],
                            [
                                'system_key' => '50b35e8451a997f1e938a3bc1a30840c',
                                'type' => 'request',
                                'desc' => '删除',
                                'method' => RequestMethod::POST,
                                'require_params' => [BuilderConst::ASYNC_TYPE_KEY => 'event', BuilderConst::ASYNC_EVENT_KEY => TableBuilderEvent::DELETED]
                            ],
                            [
                                'system_key' => 'f7362dc8ef1e83a7b629b284a5737e11',
                                'type' => 'request',
                                'desc' => '导出',
                                'method' => RequestMethod::POST,
                                'require_params' => [BuilderConst::ASYNC_TYPE_KEY => 'export']
                            ],
                            [
                                'system_key' => '4cfb2961651cd37c08deb5368ef63e41',
                                'type' => 'request',
                                'desc' => '同步系统权限',
                                'method' => RequestMethod::POST,
                                'require_params' => [BuilderConst::ASYNC_TYPE_KEY => 'syncSystemDatas']
                            ],
                            [
                                'system_key' => 'ebef27a76a979539de15ec118c94589c',
                                'type' => 'request',
                                'desc' => '清理缓存',
                                'method' => RequestMethod::POST,
                                'require_params' => [BuilderConst::ASYNC_TYPE_KEY => 'clearCache']
                            ],
                        ],
                    ],
                    [
                        'system_key' => '54a62080ba0d295d2b2dccfbf2f6a1fa',
                        'type' => 'route',
                        'name' => 'yunjAdminAuthSort',
                        'rule' => 'sort',
                        'route' => class_route([AuthController::class, 'sort']),
                        'desc' => '权限排序',
                        'items' => [
                            [
                                'system_key' => '57cff0d7b6244e52a4c3bcfb85ee0ba5',
                                'type' => 'request',
                                'desc' => '权限排序',
                            ],
                        ],
                    ],
                    [
                        'system_key' => 'fdb2f7c663957d40d72489ce1f77a7ec',
                        'type' => 'route',
                        'name' => 'yunjAdminAuthAdd',
                        'rule' => 'add',
                        'route' => class_route([AuthController::class, 'add']),
                        'desc' => '权限新增',
                        'items' => [
                            [
                                'system_key' => '0319b356821067938e98ea7cf09bcdb3',
                                'type' => 'request',
                                'desc' => '权限新增',
                            ],
                        ],
                    ],
                    [
                        'system_key' => 'c5c45e0f9f778bc4a68231d7579081c5',
                        'type' => 'route',
                        'name' => 'yunjAdminAuthEdit',
                        'rule' => 'edit',
                        'route' => class_route([AuthController::class, 'edit']),
                        'desc' => '权限编辑',
                        'items' => [
                            [
                                'system_key' => '828a46e911cc588aea140f66adf5d94f',
                                'type' => 'request',
                                'desc' => '页面',
                                'method' => RequestMethod::GET,
                            ],
                            [
                                'system_key' => '2b0365120479dee35948147f434e7ad0',
                                'type' => 'request',
                                'desc' => '数据提交',
                                'method' => RequestMethod::POST,
                            ],
                        ],
                    ],
                    [
                        'system_key' => '5e02cd38ea317da9c0b44a87b49e3df9',
                        'type' => 'route',
                        'name' => 'yunjAdminAuthDropdownsearch',
                        'rule' => 'dropdownsearch',
                        'route' => class_route([AuthController::class, 'dropdownSearch']),
                        'desc' => '权限下拉搜索',
                    ],
                    [
                        'system_key' => '34ed5ad134c090127038c42a2573a43e',
                        'type' => 'route',
                        'name' => 'yunjAdminAuthRequestDropdownsearch',
                        'rule' => 'requestDropdownsearch',
                        'route' => class_route([AuthController::class, 'requestDropdownsearch']),
                        'desc' => '权限下拉搜索路由请求项',
                    ]
                ]
            ],
        ],
    ],
    [
        'system_key' => '0f35d30b1e835c2cf89e499e782f33ea',
        'type' => 'group',
        'name' => YUNJ_ADMIN_ENTRANCE,
        'namespace' => '',
        'desc' => '后台通用',
        'enable_login_auth' => EnableLoginAuth::OFF,
        'items' => [
            [
                'system_key' => '03c44e5ce6c8ecee48444a895c1d664c',
                'type' => 'route',
                'name' => 'yunjAdminMemberLogin',
                'rule' => 'login',
                'route' => class_route([get_class(admin_member_login_controller()), 'login']),
                'desc' => '登录',
                'items' => [
                    [
                        'system_key' => '8c40774a568cafd34844a771aed46a8f',
                        'type' => 'request',
                        'desc' => '页面',
                        'method' => RequestMethod::GET,
                    ],
                    [
                        'system_key' => '0e9f0bbae51378f39b0baa59f26a3665',
                        'type' => 'request',
                        'desc' => '提交',
                        'method' => RequestMethod::POST,
                    ],
                ],
            ],
            [
                'system_key' => '6ba371aad74d66ef93dd11f522606a76',
                'type' => 'route',
                'name' => 'yunjAdminEmpty',
                'rule' => 'empty',
                'route' => class_route([OtherController::class, '_empty']),
                'desc' => '空请求',
                'items' => [
                    [
                        'system_key' => 'e6869fbf5954896f2dcd72390ac34e9e',
                        'type' => 'request',
                        'desc' => '空请求',
                    ]
                ],
            ],
            [
                'system_key' => 'aa39134b6ea55c602ac052a5dc1b77dc',
                'type' => 'route',
                'name' => 'yunjAdminTips',
                'rule' => 'tips',
                'route' => class_route([OtherController::class, 'tips']),
                'desc' => '提示页',
                'items' => [
                    [
                        'system_key' => '3b01d0adbb6ca5b7e759eb4b66eb808b',
                        'type' => 'request',
                        'desc' => '提示页',
                        'method' => RequestMethod::GET,
                    ]
                ],
            ],
            [
                'system_key' => '7c6c610c8f303b69000d8493591999c3',
                'type' => 'route',
                'name' => 'yunjAdminCaptcha',
                'rule' => 'captcha',
                'route' => class_route([OtherController::class, 'captcha']),
                'desc' => '图片验证码',
                'items' => [
                    [
                        'system_key' => 'd5946b14cb5da946ed124ab63fb9023b',
                        'type' => 'request',
                        'desc' => '图片验证码',
                        'method' => RequestMethod::POST,
                    ]
                ],
            ],
        ]
    ]
];

return $requests;