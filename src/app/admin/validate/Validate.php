<?php

namespace yunj\app\admin\validate;

use yunj\app\admin\traits\MemberHelper;
use yunj\app\admin\traits\ModelGet;
use yunj\core\Validate as YunjValidate;

abstract class Validate extends YunjValidate {

    use ModelGet, MemberHelper;
    
}