<?php

namespace yunj\app\admin\enum;

final class EnableLoginAuth extends Enum {

    const NIL = 0;

    const ON = 1;

    const OFF = 2;

    /**
     * 所有枚举的属性值
     * @return array
     */
    public static function allEnumAttrs(): array {
        return [
            self::NIL => [
                'desc' => '未配置'
            ],
            self::ON => [
                'desc' => '开启'
            ],
            self::OFF => [
                'desc' => '关闭'
            ],
        ];
    }

}