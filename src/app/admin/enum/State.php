<?php

namespace yunj\app\admin\enum;

final class State extends Enum {

    const NORMAL = 11;

    const RECYLE_BIN = 22;

    const DELETED = 33;

    /**
     * 所有枚举的属性值
     * @return array
     */
    public static function allEnumAttrs(): array {
        return [
            self::NORMAL => [
                'desc' => '正常'
            ],
            self::RECYLE_BIN => [
                'desc' => '回收站'
            ],
            self::DELETED => [
                'desc' => '已删除'
            ]
        ];
    }

    /**
     * 获取表格构建器链式操作state的值
     * @param array $extraOptions   额外的配置值
     * @return array
     */
    public static function getTableBuilderState(array $extraOptions = []) {
        $options = [];
        $allEnumAttrs = self::allEnumAttrs();
        foreach ($allEnumAttrs as $key => $val) {
            if ($key == self::DELETED) {
                continue;
            }
            $options[] = ['key' => $key, 'title' => $val['desc'] ?? ''];
        }
        return array_merge($options,$extraOptions);
    }

}