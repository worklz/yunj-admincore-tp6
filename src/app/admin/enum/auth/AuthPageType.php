<?php

namespace yunj\app\admin\enum\auth;

use yunj\app\admin\enum\Enum;

/**
 * 权限页面权限类型
 * Class RoutePageFormType
 * @package yunj\app\admin\enum
 */
final class AuthPageType extends Enum {

    const NORMAL = 'normal';

    const SIDEBAR_MENU = 'sidebar_menu';

    const TOP_MENU = 'top_menu';

    /**
     * 所有枚举的属性值
     * @return array
     */
    public static function allEnumAttrs(): array {
        return [
            self::NORMAL => [
                'title' => '常规权限',
                'dbType' => AuthType::NORMAL,
            ],
            self::SIDEBAR_MENU => [
                'title' => '侧边栏菜单',
                'dbType' => AuthType::SIDEBAR_MENU,
            ],
            self::TOP_MENU => [
                'title' => '顶部菜单',
                'dbType' => AuthType::TOP_MENU,
            ],
        ];
    }

    // 获取对应的数据库type值
    public function getDbType() {
        return $this->getAttr('dbType');
    }

    // 是否为菜单类型
    public function isMenu(): bool {
        return in_array($this->getValue(), [self::SIDEBAR_MENU, self::TOP_MENU]);
    }

}