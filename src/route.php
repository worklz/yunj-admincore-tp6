<?php
// +----------------------------------------------------------------------
// | 云静Admin
// +----------------------------------------------------------------------
// | Copyright (c) 2019-2022 http://www.iyunj.cn
// +----------------------------------------------------------------------
// | 云静Admin提供个人非商业用途免费使用。
// +----------------------------------------------------------------------
// | Author: Uncle-L <1071446619@qq.com>
// +----------------------------------------------------------------------
// | 路由文件
// +----------------------------------------------------------------------

use think\facade\Route;
use yunj\core\controller\FileController;
use yunj\core\controller\ErrorController;
use yunj\core\controller\CaptchaController;

// 首页地址
if (!defined('YUNJ_ADMIN_URL')) define('YUNJ_ADMIN_URL', build_url(YUNJ_ADMIN_ENTRANCE));
