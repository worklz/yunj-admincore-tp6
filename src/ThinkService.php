<?php

namespace yunj;

use yunj\command\Init;
use yunj\command\RedisClear;
use yunj\command\AdminReset;
use yunj\command\RouteFileReset;
use yunj\command\InitSystemAuths;
use yunj\command\InitSystemRoutes;

class ThinkService extends \think\Service {

    public function register() {
        
    }

    public function boot() {
        if ($this->app->runningInConsole()) {
            $this->commands([
                Init::class,
                InitSystemAuths::class,
                InitSystemRoutes::class,
                RouteFileReset::class,
                AdminReset::class,
                RedisClear::class,
            ]);
        }
    }

}
