<?php
// +----------------------------------------------------------------------
// | 云静Admin
// +----------------------------------------------------------------------
// | Copyright (c) 2019-2022 http://www.iyunj.cn
// +----------------------------------------------------------------------
// | 云静Admin提供个人非商业用途免费使用。
// +----------------------------------------------------------------------
// | Author: Uncle-L <1071446619@qq.com>
// +----------------------------------------------------------------------
// | CORS跨域请求配置，默认文件内容
// +----------------------------------------------------------------------

return [
    "enable" => <<<INFO
    // 启用
    'enable' => true,
INFO
    ,
    "access_control_allow_origin" => <<<INFO
    // 允许跨域访问的URL（为空时默认 * 允许所有的域来访问）
    'access_control_allow_origin' => '*',
INFO
    ,
    "access_control_allow_methods" => <<<INFO
    // 允许使用的methods（默认允许：POST,GET,PUT,DELETE）
    'access_control_allow_methods' => '',
INFO
    ,
    "access_control_allow_headers" => <<<INFO
    // 允许携带的headers键值对（默认允许：token,Origin,X-Requested-With,Content-Type,Accept）
    'access_control_allow_headers' => '',
INFO
    ,
];