<?php

namespace yunj\init;

// 云静Admin安装
// php vendor/yunj/admincore-tp6/src/init/run.php
use think\App;

ini_set("display_errors", "On");//打开错误提示
ini_set("error_reporting", E_ALL);//显示所有错误

define("PROJECT_ROOT_PATH", dirname(dirname(dirname(dirname(dirname(dirname(__FILE__)))))));
// 加载基础文件
require PROJECT_ROOT_PATH . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';
// 引用初始化
(new App())->initialize();

// 执行
(new Init())->handle();
