
DROP TABLE IF EXISTS `__PREFIX__yunj_demo_interest_cate`;
CREATE TABLE `__PREFIX__yunj_demo_interest_cate`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pid` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `name` varchar(255) CHARACTER SET __CHARSET__ COLLATE __CHARSET___general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = __CHARSET__ COLLATE = __CHARSET___general_ci COMMENT = '兴趣分类' ROW_FORMAT = Dynamic;

DROP TABLE IF EXISTS `__PREFIX__yunj_demo_user`;
CREATE TABLE `__PREFIX__yunj_demo_user`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `avatar` varchar(255) CHARACTER SET __CHARSET__ COLLATE __CHARSET___general_ci NOT NULL DEFAULT '' COMMENT '头像',
  `name` varchar(255) CHARACTER SET __CHARSET__ COLLATE __CHARSET___general_ci NOT NULL COMMENT '姓名',
  `sex` varchar(10) CHARACTER SET __CHARSET__ COLLATE __CHARSET___general_ci NOT NULL COMMENT 'man 男性、woman 女性',
  `age` tinyint(3) UNSIGNED NOT NULL COMMENT '年龄',
  `grade` tinyint(1) UNSIGNED NOT NULL COMMENT '年级：1 一年级、2 二年级、3 三年级、4 四年级',
  `hobby` varchar(255) CHARACTER SET __CHARSET__ COLLATE __CHARSET___general_ci NOT NULL COMMENT '爱好：read 阅读，write 写作',
  `province` char(6) CHARACTER SET __CHARSET__ COLLATE __CHARSET___general_ci NOT NULL DEFAULT '' COMMENT '省',
  `city` char(6) CHARACTER SET __CHARSET__ COLLATE __CHARSET___general_ci NOT NULL DEFAULT '' COMMENT '市',
  `district` char(6) CHARACTER SET __CHARSET__ COLLATE __CHARSET___general_ci NOT NULL DEFAULT '' COMMENT '区/县',
  `intro` varchar(255) CHARACTER SET __CHARSET__ COLLATE __CHARSET___general_ci NOT NULL COMMENT '个人介绍',
  `color` varchar(8) CHARACTER SET __CHARSET__ COLLATE __CHARSET___general_ci NOT NULL DEFAULT '' COMMENT '喜欢的颜色',
  `album` varchar(2000) CHARACTER SET __CHARSET__ COLLATE __CHARSET___general_ci NOT NULL DEFAULT '' COMMENT '相册',
  `interest_cate_ids` varchar(255) CHARACTER SET __CHARSET__ COLLATE __CHARSET___general_ci NOT NULL DEFAULT '' COMMENT '兴趣分类',
  `friend_ids` varchar(128) CHARACTER SET __CHARSET__ COLLATE __CHARSET___general_ci NOT NULL DEFAULT '' COMMENT '好友ids',
  `document` varchar(500) CHARACTER SET __CHARSET__ COLLATE __CHARSET___general_ci NOT NULL DEFAULT '' COMMENT '文件',
  `other_document` varchar(500) CHARACTER SET __CHARSET__ COLLATE __CHARSET___general_ci NOT NULL DEFAULT '' COMMENT '其他文件',
  `sort` int(11) UNSIGNED NOT NULL DEFAULT 99 COMMENT '排序',
  `create_time` int(10) UNSIGNED NOT NULL COMMENT '添加时间',
  `last_update_time` int(10) UNSIGNED NOT NULL COMMENT '更新时间',
  `state` tinyint(4) UNSIGNED NOT NULL DEFAULT 11 COMMENT '11 正常 | 22 回收站 | 33 已删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = __CHARSET__ COLLATE = __CHARSET___general_ci COMMENT = '示例表' ROW_FORMAT = Dynamic;

INSERT INTO `__PREFIX__yunj_demo_user` VALUES (1, '', '某某人1', 'man', 17, 1, '', '210000', '210100', '210102', '某人的个人介绍', '#000000', '', '', '', '', '', 99, 1673431704, 1673431704, 11);
INSERT INTO `__PREFIX__yunj_demo_user` VALUES (2, '', '某某人2', 'man', 18, 1, 'read', '', '', '', '个人简介', '', '', '', '', '', '', 99, 1675483624, 1675483624, 11);
INSERT INTO `__PREFIX__yunj_demo_user` VALUES (3, '', '某某人3', 'man', 19, 2, 'read', '', '', '', '个人简介', '', '', '', '', '', '', 99, 1675483624, 1675483624, 11);
INSERT INTO `__PREFIX__yunj_demo_user` VALUES (4, '', '某某人4', 'man', 20, 3, 'read', '', '', '', '个人简介', '', '', '', '', '', '', 99, 1675483624, 1675483624, 11);
INSERT INTO `__PREFIX__yunj_demo_user` VALUES (5, '', '某某人5', 'man', 21, 4, 'read', '', '', '', '个人简介', '', '', '', '', '', '', 99, 1675483624, 1675484022, 22);
