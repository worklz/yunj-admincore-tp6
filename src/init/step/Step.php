<?php

namespace yunj\init\step;

use think\facade\Db;
use yunj\init\traits\Output;

abstract class Step {

    use Output;

    // 校验结果：成功
    const RES_SUCCESS = 1;
    // 校验结果：失败
    const RES_FAIL = 2;
    // 校验结果：跳过
    const RES_SKIP = 3;

    /**
     * 描述
     * @var string
     */
    protected $desc;

    public function __construct(string $desc) {
        $this->desc = $desc;
    }

    /**
     * @return mixed
     */
    public abstract function handle();

    /**
     * 获取数据库连接配置
     * @param string $attr 属性名
     * @return array|mixed
     */
    protected function getDbConnConfig(string $attr = '', $def = null) {
        static $dbConnConfig;
        if (!$dbConnConfig) $dbConnConfig = Db::connect()->getConfig();
        return $attr ? ($dbConnConfig[$attr] ?? $def) : $dbConnConfig;
    }

    /**
     * 获取数据库编码
     * @return string|mixed
     */
    protected function getDbCharset() {
        return $this->getDbConnConfig('charset', 'utf8');
    }

}