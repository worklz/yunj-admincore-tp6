<?php

namespace yunj\command;

use think\console\Input;
use think\console\Output;
use yunj\core\Command as BaseCommand;
use yunj\app\admin\service\route\RouteSyncSystemDataService;

class InitSystemRoutes extends BaseCommand {

    protected function configure() {
        $this->setName('yunj:init-system-routes')
            ->setDescription('云静初始化系统路由');
    }

    protected function execute(Input $input, Output $output) {
        ini_set("display_errors", "On");//打开错误提示
        ini_set("error_reporting", E_ALL);//显示所有错误
        set_time_limit(3600);

        try {
            RouteSyncSystemDataService::handle();
            $this->success();
        } catch (\Throwable $e) {
            $this->error(exception_to_str($e));
        }
    }
}