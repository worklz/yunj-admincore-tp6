<?php

namespace yunj\command;

use think\console\Input;
use think\console\Output;
use yunj\app\admin\service\auth\AuthService;
use yunj\core\Command as BaseCommand;

class InitSystemAuths extends BaseCommand {

    protected function configure() {
        $this->setName('yunj:init-system-auths')
            ->setDescription('云静初始化系统权限');
    }

    protected function execute(Input $input, Output $output) {
        ini_set("display_errors", "On");//打开错误提示
        ini_set("error_reporting", E_ALL);//显示所有错误
        try {
            AuthService::syncSystemDatas();
            $this->success();
        } catch (\Throwable $e) {
            $this->error(exception_to_str($e));
        }
    }
}