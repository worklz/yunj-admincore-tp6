<?php
// +----------------------------------------------------------------------
// | 云静Admin
// +----------------------------------------------------------------------
// | Copyright (c) 2019-2022 http://www.iyunj.cn
// +----------------------------------------------------------------------
// | 云静Admin提供个人非商业用途免费使用。
// +----------------------------------------------------------------------
// | Author: Uncle-L <1071446619@qq.com>
// +----------------------------------------------------------------------
// | 自定义组件配置
// +----------------------------------------------------------------------

return [

    // 表单字段类型
    'fields'=>[
        // 自定义字段
        'custom'=>[
            'args'=>'\\yunj\\core\\control\\form\\field\\Custom',
            'module'=>'/static/yunj/js/modules/form/field/custom.min',
        ],
        // 隐藏域
        'hidden'=>[
            'args'=>'\\yunj\\core\\control\\form\\field\\Hidden',
            'module'=>'/static/yunj/js/modules/form/field/hidden.min',
        ],
        // 文本框
        'text'=>[
            'args'=>'\\yunj\\core\\control\\form\\field\\Text',
            'module'=>'/static/yunj/js/modules/form/field/text.min',
        ],
        // 文本域
        'textarea'=>[
            'args'=>'\\yunj\\core\\control\\form\\field\\Textarea',
            'module'=>'/static/yunj/js/modules/form/field/textarea.min',
        ],
        // 密码框
        'password'=>[
            'args'=>'\\yunj\\core\\control\\form\\field\\Password',
            'module'=>'/static/yunj/js/modules/form/field/password.min',
        ],
        // 富文本
        'editor'=>[
            'args'=>'\\yunj\\core\\control\\form\\field\\Editor',
            'module'=>'/static/yunj/js/modules/form/field/editor.min',
        ],
        // 文档编辑
        'markdown'=>[
            'args'=>'\\yunj\\core\\control\\form\\field\\Markdown',
            'module'=>'/static/yunj/js/modules/form/field/markdown.min',
        ],
        // 下拉选框
        'select'=>[
            'args'=>'\\yunj\\core\\control\\form\\field\\Select',
            'module'=>'/static/yunj/js/modules/form/field/select.min',
        ],
        // 单选框
        'radio'=>[
            'args'=>'\\yunj\\core\\control\\form\\field\\Radio',
            'module'=>'/static/yunj/js/modules/form/field/radio.min',
        ],
        // 复选框
        'checkbox'=>[
            'args'=>'\\yunj\\core\\control\\form\\field\\Checkbox',
            'module'=>'/static/yunj/js/modules/form/field/checkbox.min',
        ],
        // 开关
        'switch'=>[
            'args'=>'\\yunj\\core\\control\\form\\field\\YSwitch',
            'module'=>'/static/yunj/js/modules/form/field/switch.min',
        ],
        // 日期
        'date'=>[
            'args'=>'\\yunj\\core\\control\\form\\field\\Date',
            'module'=>'/static/yunj/js/modules/form/field/date.min',
        ],
        // 时间日期
        'datetime'=>[
            'args'=>'\\yunj\\core\\control\\form\\field\\Datetime',
            'module'=>'/static/yunj/js/modules/form/field/datetime.min',
        ],
        // 年
        'year'=>[
            'args'=>'\\yunj\\core\\control\\form\\field\\Year',
            'module'=>'/static/yunj/js/modules/form/field/year.min',
        ],
        // 月
        'yearMonth'=>[
            'args'=>'\\yunj\\core\\control\\form\\field\\YearMonth',
            'module'=>'/static/yunj/js/modules/form/field/year-month.min',
        ],
        // 时间
        'time'=>[
            'args'=>'\\yunj\\core\\control\\form\\field\\Time',
            'module'=>'/static/yunj/js/modules/form/field/time.min',
        ],
        // 图片
        'image'=>[
            'args'=>'\\yunj\\core\\control\\form\\field\\Image',
            'module'=>'/static/yunj/js/modules/form/field/image.min',
        ],
        // 文件
        'file'=>[
            'args'=>'\\yunj\\core\\control\\form\\field\\File',
            'module'=>'/static/yunj/js/modules/form/field/file.min',
        ],
        // 取色器
        'color'=>[
            'args'=>'\\yunj\\core\\control\\form\\field\\Color',
            'module'=>'/static/yunj/js/modules/form/field/color.min',
        ],
        // 地区联动
        'area'=>[
            'args'=>'\\yunj\\core\\control\\form\\field\\Area',
            'module'=>'/static/yunj/js/modules/form/field/area.min',
        ],
        // 下拉搜索
        'dropdownSearch'=>[
            'args'=>'\\yunj\\core\\control\\form\\field\\DropdownSearch',
            'module'=>'/static/yunj/js/modules/form/field/dropdown-search.min',
        ],
        // 树形
        'tree'=>[
            'args'=>'\\yunj\\core\\control\\form\\field\\Tree',
            'module'=>'/static/yunj/js/modules/form/field/tree.min',
        ],
        // 分割线
        'line'=>[
            'args'=>'\\yunj\\core\\control\\form\\field\\Line',
            'module'=>'/static/yunj/js/modules/form/field/line.min',
        ],
        // 文字
        'txt'=>[
            'args'=>'\\yunj\\core\\control\\form\\field\\Txt',
            'module'=>'/static/yunj/js/modules/form/field/txt.min',
        ],
        // 图标
        'icon'=>[
            'args'=>'\\yunj\\core\\control\\form\\field\\Icon',
            'module'=>'/static/yunj/js/modules/form/field/icon.min',
        ],
        // 表格
        'table'=>[
            'args'=>'\\yunj\\core\\control\\form\\field\\Table',
            'module'=>'/static/yunj/js/modules/form/field/table.min',
        ],
    ],

    // 表格表头模板
    'cols'=>[
        // 自定义表头
        'custom'=>[
            'args'=>'\\yunj\\core\\control\\table\\cols\\templet\\Custom',
            'module'=>'/static/yunj/js/modules/table/templet/cols/custom.min',
        ],
        // 操作栏
        'action'=>[
            'args'=>'\\yunj\\core\\control\\table\\cols\\templet\\Action',
            'module'=>'/static/yunj/js/modules/table/templet/cols/action.min',
        ],
        // 拖拽排序
        'dragSort'=>[
            'args'=>'\\yunj\\core\\control\\table\\cols\\templet\\DragSort',
            'module'=>'/static/yunj/js/modules/table/templet/cols/drag-sort.min',
        ],
        // 单图预览
        'datetime'=>[
            'args'=>'\\yunj\\core\\control\\table\\cols\\templet\\Datetime',
            'module'=>'/static/yunj/js/modules/table/templet/cols/datetime.min',
        ],
        // 图片预览
        'image'=>[
            'args'=>'\\yunj\\core\\control\\table\\cols\\templet\\Image',
            'module'=>'/static/yunj/js/modules/table/templet/cols/image.min',
        ],
        // 文件预览
        'file'=>[
            'args'=>'\\yunj\\core\\control\\table\\cols\\templet\\File',
            'module'=>'/static/yunj/js/modules/table/templet/cols/file.min',
        ],
        // 颜色呈现
        'color'=>[
            'args'=>'\\yunj\\core\\control\\table\\cols\\templet\\Color',
            'module'=>'/static/yunj/js/modules/table/templet/cols/color.min',
        ],
        // 地区呈现
        'area'=>[
            'args'=>'\\yunj\\core\\control\\table\\cols\\templet\\Area',
            'module'=>'/static/yunj/js/modules/table/templet/cols/area.min',
        ],
        // 枚举
        'enum'=>[
            'args'=>'\\yunj\\core\\control\\table\\cols\\templet\\Enum',
            'module'=>'/static/yunj/js/modules/table/templet/cols/enum.min',
        ],
        // json
        'json'=>[
            'args'=>'\\yunj\\core\\control\\table\\cols\\templet\\Json',
            'module'=>'/static/yunj/js/modules/table/templet/cols/json.min',
        ],
        // icon
        'icon'=>[
            'args'=>'\\yunj\\core\\control\\table\\cols\\templet\\Icon',
            'module'=>'/static/yunj/js/modules/table/templet/cols/icon.min',
        ],
    ],

];