<?php
// +----------------------------------------------------------------------
// | 云静Admin
// +----------------------------------------------------------------------
// | Copyright (c) 2019-2022 http://www.iyunj.cn
// +----------------------------------------------------------------------
// | 云静Admin提供个人非商业用途免费使用。
// +----------------------------------------------------------------------
// | Author: Uncle-L <1071446619@qq.com>
// +----------------------------------------------------------------------
// | 设置配置
// +----------------------------------------------------------------------

return [

    // 分组
    // 配置/调整完成后需进入项目根目录执行命令：php think yunj:init-system-auths 重置系统权限（或进入权限管理页面执行列表右上角系统权限同步功能）
    'groups' => [
        'sys' => [
            'title' => '系统设置',
            'icon' => 'layui-icon-set',
            // 表单
            'form' => [
                // 字段权限
                'field_auths' => [
                    'set_sys_field_normal' => [
                        'desc' => '普通字段',
                    ],
                    // 基础配置
                    'set_sys_base_field' => [
                        'desc' => '基础配置',
                    ],
                    'set_sys_field_title' => [
                        'desc' => '系统标题',
                        'parent' => 'set_sys_base_field',
                    ],
                    'set_sys_field_keywords' => [
                        'desc' => '系统关键词',
                        'parent' => 'set_sys_base_field',
                    ],
                    'set_sys_field_description' => [
                        'desc' => '系统描述',
                        'parent' => 'set_sys_base_field',
                    ],
                    'set_sys_field_favicon' => [
                        'desc' => '站点图标（favicon）',
                        'parent' => 'set_sys_base_field',
                    ],
                    'set_sys_field_default_img' => [
                        'desc' => '默认图',
                        'parent' => 'set_sys_base_field',
                    ],
                    // 文件配置
                    'set_sys_file_field' => [
                        'desc' => '文件配置',
                    ],
                    // 七牛云配置
                    'set_sys_qiniu_field' => [
                        'desc' => '七牛云配置',
                    ],
                    'set_sys_field_qiniu_is_enable' => [
                        'desc' => '开启',
                        'parent' => 'set_sys_qiniu_field',
                    ],
                    'set_sys_field_qiniu_other' => [
                        'desc' => '其他配置',
                        'parent' => 'set_sys_qiniu_field',
                    ],
                ],
                // 字段默认值
                'field_default' => [
                    'title' => '云静Admin TP6',
                    'keywords' => '云静Admin,后台,yunj,admin,thinkphp',
                    'description' => '云静Admin TP6',
                    'favicon' => '/favicon.ico',
                    'default_img' => \yunj\core\enum\Def::IMG_URI,
                    'upload_img_ext' => 'ico,jpg,png,gif,jpeg',
                    'upload_img_size' => 1,
                    'upload_file_ext' => 'txt,pdf,xlsx,xls,csv',
                    'upload_file_size' => 5,
                    'upload_media_ext' => 'avi,mkv,mp3,mp4',
                    'upload_media_size' => 20,
                    'qiniu_is_enable' => 'off',
                    'qiniu_access_key' => '',
                    'qiniu_secret_key' => '',
                    'qiniu_bucket' => '',
                    'qiniu_domain' => '',
                ],
                // 表单构建器切换栏
                'tab' => ['base' => '基础配置', 'file' => '文件配置', 'qiniu' => '七牛云配置'],
                // 表单构建器字段
                'field' => function (\yunj\core\builder\YunjForm $builder, $tab) {
                    $field = [];
                    switch ($tab) {
                        case 'base':
                            $field = [
                                'title' => [
                                    "title" => "系统标题",
                                    "auth" => "set_sys_field_title",
                                    "verify" => "require",
                                ],
                                'keywords' => [
                                    "title" => "系统关键词",
                                    "auth" => "set_sys_field_keywords",
                                ],
                                'description' => [
                                    "title" => "系统描述",
                                    "auth" => "set_sys_field_description",
                                ],
                                'favicon' => [
                                    "title" => "站点图标（favicon）",
                                    "type" => "image",
                                    "auth" => "set_sys_field_favicon",
                                ],
                                'default_img' => [
                                    "title" => "默认图",
                                    "type" => "image",
                                    "auth" => "set_sys_field_default_img",
                                    "verify" => "require"
                                ]
                            ];
                            break;
                        case 'file':
                            $field = [
                                "upload_line" => [
                                    "title" => "上传",
                                    "type" => "line",
                                ],
                                'upload_img_ext' => [
                                    "title" => "图片允许上传后缀",
                                    "auth" => "set_sys_file_field",
                                ],
                                'upload_img_size' => [
                                    "title" => "图片允许上传大小",
                                    "auth" => "set_sys_file_field",
                                    "desc" => '单位MB',
                                ],
                                'upload_file_ext' => [
                                    "title" => "文件允许上传后缀",
                                    "auth" => "set_sys_file_field",
                                ],
                                'upload_file_size' => [
                                    "title" => "文件允许上传大小",
                                    "auth" => "set_sys_file_field",
                                    "desc" => '单位MB',
                                ],
                                'upload_media_ext' => [
                                    "title" => "媒体允许上传后缀",
                                    "auth" => "set_sys_file_field",
                                ],
                                'upload_media_size' => [
                                    "title" => "媒体允许上传大小",
                                    "auth" => "set_sys_file_field",
                                    "desc" => '单位MB',
                                ],
                            ];
                            break;
                        case 'qiniu':
                            $field = [
                                'qiniu_is_enable' => [
                                    "title" => "开启",
                                    "type" => "switch",
                                    "auth" => "set_sys_field_qiniu_is_enable",
                                    "desc" => '开启后文件上传将走七牛云途径上传',
                                ],
                                'qiniu_domain' => [
                                    "title" => "域名",
                                    "auth" => "set_sys_field_qiniu_normal",
                                    "desc" => '示例：https://xxx.com',
                                ],
                                'qiniu_bucket' => [
                                    "title" => "存储空间（bucket）",
                                    "auth" => "set_sys_field_qiniu_normal",
                                ],
                                'qiniu_access_key' => [
                                    "title" => "AccessKey",
                                    "auth" => "set_sys_field_qiniu_normal",
                                ],
                                'qiniu_secret_key' => [
                                    "title" => "SecretKey",
                                    "auth" => "set_sys_field_qiniu_normal",
                                ],
                            ];
                            break;
                    }
                    // 设置栅格布局
                    foreach ($field as &$v) {
                        $v['grid'] = [12, "6 r6", "6 r6"];
                    }
                    return $field;
                },
                'validate' => \yunj\core\validate\YunjSettingValidate::class,
            ]
        ]
    ]

];