<?php
// +----------------------------------------------------------------------
// | 云静Admin
// +----------------------------------------------------------------------
// | Copyright (c) 2019-2022 http://www.iyunj.cn
// +----------------------------------------------------------------------
// | 云静Admin提供个人非商业用途免费使用。
// +----------------------------------------------------------------------
// | Author: Uncle-L <1071446619@qq.com>
// +----------------------------------------------------------------------
// | 表格配置
// +----------------------------------------------------------------------

return [

    // defaultToolbar的有效值（filter列显示/隐藏、print打印、export导出）
    'defaultToolbar'=>['filter','print','export'],

    // 导入
    'import'=>[
        // 默认提示
        'def_tips'=>[
            '限制文件上传格式：xlsx/xls/csv',
            '上传数据不能包含图片、视频等媒体资源',
            '限制使用合并单元格存放数据',
        ],
    ]
];