<?php
// +----------------------------------------------------------------------
// | 云静Admin
// +----------------------------------------------------------------------
// | Copyright (c) 2019-2022 http://www.iyunj.cn
// +----------------------------------------------------------------------
// | 云静Admin提供个人非商业用途免费使用。
// +----------------------------------------------------------------------
// | Author: Uncle-L <1071446619@qq.com>
// +----------------------------------------------------------------------
// | 注意：
// | 1、配置 'url_common_param'=>true
// +----------------------------------------------------------------------
// | 初始化（禁止更改代码顺序）
// +----------------------------------------------------------------------

namespace yunj\core\listener;

use think\facade\View;

class HttpRun {

    public function handle() {
        View::assign([
            "yunjInit" => yunj_vender_src_view_template("yunj_init"),
            "adminPage" => yunj_vender_src_view_template("admin/page"),
        ]);
    }

}