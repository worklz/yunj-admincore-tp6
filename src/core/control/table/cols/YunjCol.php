<?php

namespace yunj\core\control\table\cols;

class YunjCol {

    const REQUIRE_ARGS = [
        'field' => '',              // 设定字段名
        'title' => '',              // 标题（可选）
        'type' => 'normal',         // 列类型（可选，可选值：normal,checkbox,data）
        'align' => 'left',          // 排列方式（可选，可选值：left,center,right）
        'sort' => false,            // 是否允许排序（可选，默认：false）
        'fixed' => false,           // 固定列（可选，可选值：left,right）
        'hide' => false,            // 初始隐藏列（可选，默认：false。可选值：bool值，yes,no,mobileHide。注意：y等价于yes、n等价于no，大小写不敏感）
        'templet' => '',            // 自定义列模板（可选，可选值：code）
        'auth' => '',               // 权限key
    ];

    /**
     * 校验
     * @return array<error,args>    [错误消息,正常参数]
     */
    public static function check(string $key, array $args): array {
        try {
            $args = static::setArgs($key, $args);
            // auth 权限校验
            if (isset($args['auth']) && $args['auth'] && !admin_member_auth_check($args['auth'])) $args = [];
            return [null, $args];
        } catch (GeneralException $e) {
            return [$e->getMessage(), null];
        }
    }

    /**
     * @param string $key
     * @param array $args
     * @return array
     */
    protected static function setArgs(string $key, array $args): array {
        $args = array_supp($args, static::defineExtraArgs() + static::REQUIRE_ARGS);
        $args = static::handleArgs($args);
        if (!$args) return [];
        $args['field'] = $key;
        $templet = $args['templet'];
        if ($templet && !is_string($templet) && is_callable($templet)) $args['templet'] = call_user_func($templet);
        return $args;
    }

    /**
     * 定义额外参数
     * @return array
     */
    protected static function defineExtraArgs(): array {
        return [];
    }

    /**
     * 处理 args
     * @param array $args
     * @return array
     */
    protected static function handleArgs(array $args): array {
        return $args;
    }

}