<?php

namespace yunj\core\control\table\cols\templet;

use yunj\core\control\table\cols\YunjCol;

class Custom extends YunjCol {

    protected static function defineExtraArgs(): array {
        return [
            'html' => '',     // 自定义的html
        ];
    }

}