<?php

namespace yunj\core\control\table\cols\templet;

use yunj\core\control\table\cols\YunjCol;

class Area extends YunjCol {

    protected static function defineExtraArgs(): array {
        return [
            'acc' => 'district',     // 精度，可选值：province、city、district
        ];
    }

}