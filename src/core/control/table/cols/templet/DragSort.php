<?php

namespace yunj\core\control\table\cols\templet;

use yunj\core\control\table\cols\YunjCol;

class DragSort extends YunjCol {

    protected static function defineExtraArgs(): array {
        return [
            'iconClass' => '',
        ];
    }

    protected static function handleArgs(array $args): array {
        if (!$args["iconClass"]) $args["iconClass"] = "yunj-icon-sort-circle";
        return $args;
    }

}