<?php

namespace yunj\core\control\table;

use yunj\core\Config;
use yunj\core\exception\GeneralException;

final class YunjTableToolbar {
    
    const DEFAULT_REQUIRE_ARGS = [
        'type' => '',                 // 触发事件类型（openTab 打开子页面、openPopup 打开弹出层页面、asyncEvent 异步事件）
        'title' => '',                // 标题
        'class' => '',                // 额外的class或图标类名
        'url' => '',                  // 触发事件执行的url
        'confirmText' => '',          // 确认弹窗提示
        'dropdown' => false,          // 是否下拉操作
        'auth' => '',                 // 权限key
    ];


    /**
     * 校验
     * @return array<error,args>    [错误消息,正常参数]
     */
    public static function check(array $args): array {
        try {
            $args = array_supp($args, self::DEFAULT_REQUIRE_ARGS);
            // type
            if (in_array($args['type'], ['openTab', 'openPopup'])) {
                if (!$args['url']) throw new GeneralException("YunjTable [toolbar] 配置 [type]='openTab'或'openPopup'时，[url] 不能为空");
            }
            // class
            $args['class'] = icon_class($args['class']);
            // auth 判断是否有权限，没有权限则返回空数组
            if ($args['auth'] && !admin_member_auth_check($args['auth'])) $args = [];
            return [null, $args];
        } catch (GeneralException $e) {
            return [$e->getMessage(), null];
        }
    }

}