<?php

namespace yunj\core\control\form\field;

class Icon extends YunjField {

    protected static function defineExtraArgs(): array {
        return [
            'placeholder' => "图标class类名",
        ];
    }

}