<?php
namespace yunj\core\control\form\field;

class Textarea extends YunjField {

    protected static function defineExtraArgs():array{
        return [
            'placeholder' => '',     // 占位符
        ];
    }

}