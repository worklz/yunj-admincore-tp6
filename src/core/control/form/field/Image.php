<?php

namespace yunj\core\control\form\field;

class Image extends YunjField {

    protected static function defineExtraArgs(): array {
        return [
            'multi' => false,                                     // 是否多图，默认false，单位true是表示多图上传，默认最多上传9张图片。传入数字时，即表示多图上传，最多允许上传限制的数字图片张数
            'size' => yunj_setting('sys.upload_img_size'),    // 限制上传图片大小，默认配置值
            'ext' => yunj_setting('sys.upload_img_ext'),      // 限制上传图片后缀，默认配置值
            'text' => "图片上传",                                   // 显示文字
        ];
    }

    protected static function handleArgs(array $args): array {
        // verify
        if ($args['multi']) {
            if (!strstr($args["verify"], "urls"))
                $args["verify"] .= ($args["verify"] ? "|" : "") . "urls";
        } else {
            if (!strstr($args["verify"], "url"))
                $args["verify"] .= ($args["verify"] ? "|" : "") . "url";
        }
        return $args;
    }

}