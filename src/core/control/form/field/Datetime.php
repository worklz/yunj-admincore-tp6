<?php

namespace yunj\core\control\form\field;

// 格式yyyy-MM-dd HH:mm:ss
class Datetime extends Date {

    protected static function handleArgs(array $args): array {
        // verify
        self::handleArgsVerify($args);
        return $args;
    }

}