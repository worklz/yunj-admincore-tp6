<?php

namespace yunj\core\control\form\field;

// 格式yyyy
class Year extends Date {

    protected static function handleArgs(array $args): array {
        // verify
        self::handleArgsVerify($args);
        return $args;
    }

}