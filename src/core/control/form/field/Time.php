<?php

namespace yunj\core\control\form\field;

// 格式HH:mm:ss
class Time extends Date {

    protected static function handleArgs(array $args): array {
        // verify
        self::handleArgsVerify($args);
        return $args;
    }

}