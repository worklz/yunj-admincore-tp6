<?php

namespace yunj\core\control\form\field;

use yunj\core\exception\GeneralException;

class YSwitch extends YunjField {

    protected static function defineExtraArgs(): array {
        return [
            'text' => '开|关',        // 显示文本（可选，定义两种状态的文本，前者表示开，后者代表关，默认 开|关）
            'textValue' => 'on|off'    // 显示文本对应值（可选，定义两种状态的值，前者表示开，后者代表关，默认 on|off）
        ];
    }

    protected static function handleArgs(array $args): array {
        // text
        $text = $args['text'];
        if (!strstr($text, '|')) {
            throw new GeneralException('类型[switch]配置[text]错误');
        }
        // textValue
        $textValue = $args['textValue'];
        if (!strstr($text, '|')) {
            throw new GeneralException('类型[switch]配置[textValue]错误');
        }
        // verify
        if (!strstr($args["verify"], "in")) {
            list($on, $off) = explode('|', $textValue);
            $args["verify"] .= ($args["verify"] ? "|" : "") . "in:{$on},{$off}";
        }

        return $args;
    }

}