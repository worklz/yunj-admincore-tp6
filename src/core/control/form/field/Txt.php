<?php

namespace yunj\core\control\form\field;

class Txt extends YunjField {

    protected static function defineExtraArgs(): array {
        return [
            'align' => "center",    // 排列。left 居左、center 居中、right 居右
            'color' => '',          // 颜色css:color
            'size' => '',           // 文字大小css:font-size
            'weight' => 600,        // 对应css:font-weight
        ];
    }

    protected static function handleArgs(array $args): array {
        $args["readonly"] = true;
        if (!in_array($args['align'], ['left', 'center', 'right'])) {
            throw new GeneralException('类型[txt]配置[align]错误');
        }
        return $args;
    }

}