<?php

namespace yunj\core\enum;

final class BuilderType extends Enum {

    // 表单
    const FORM = 'yunj_form';

    // 表格
    const TABLE = 'yunj_table';

    // 导入
    const IMPORT = 'yunj_import';

}