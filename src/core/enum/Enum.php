<?php

namespace yunj\core\enum;

use yunj\library\enum\Enum as YunjEnum;

abstract class Enum extends YunjEnum {

    /**
     * 表单开关字段参数
     * @return array
     */
    public static function formFieldSwitchArgs(): array {
        $descOptions = static::getDescOptions();
        $vals = array_keys($descOptions);
        return [
            'text' => $descOptions[$vals[0]] . '|' . $descOptions[$vals[1]],
            'textValue' => $vals[0] . '|' . $vals[1]
        ];
    }

}