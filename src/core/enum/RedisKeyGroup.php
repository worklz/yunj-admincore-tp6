<?php

namespace yunj\core\enum;

use yunj\core\traits\RedisKey as RedisKeyTrait;
use yunj\core\traits\RedisKeyGroup as RedisKeyGroupTrait;

class RedisKeyGroup extends Enum {

    use RedisKeyGroupTrait;

    protected function redisKeyEnum() {
        return RedisKey::class;
    }

}
