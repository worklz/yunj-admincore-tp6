<?php

namespace yunj\core\enum;

/**
 * 默认值
 * Class Def
 * @package yunj\core\enum
 */
final class Def extends Enum {

    // 项目启动年
    const PROJECT_START_YEAR = '2019';

    // 项目启动年月
    const PROJECT_START_YEAR_MONTH = self::PROJECT_START_YEAR . '-06';

    // 项目启动日期
    const PROJECT_START_DATE = self::PROJECT_START_YEAR_MONTH . '-15';

    // 项目启动日期时间
    const PROJECT_START_DATETIME = self::PROJECT_START_DATE . ' 00:00:00';

    // 表单时间日期字段范围分隔符
    const FORM_FIELD_DATE_RANGE = '-';
    
    // 默认图片地址
    const IMG_URI = '/static/yunj/img/default.png';

    // 测试文件地址
    const TEST_FILE_URL = '/static/yunj/test.txt';

    // 成功提示
    const SUCCESS_MSG = '操作成功';

    // 确认errcode
    const CONFIRM_ERRCODE = 1;

    // 确认弹窗（确认后的提交的已确认参数header key）
    const CONFIRMED_HEADER_KEY = 'CONFIRMED';

    // 列表页面每页显示数据条数
    const LIST_PAGE_LIMIT = 20;

}