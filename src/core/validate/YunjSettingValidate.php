<?php

namespace yunj\core\validate;

use yunj\core\Validate;
use yunj\app\admin\controller\member\MemberLoginControllerInterface;

class YunjSettingValidate extends Validate {

    /**
     * 登录控制器校验
     * @param $value [验证数据]
     * @param string $rule [验证规则]
     * @param array $data [全部数据]
     * @param string $field [字段名]
     * @param string $title [字段描述]
     * @return bool|string
     */
    protected function isLoginController($value, string $rule = "", array $data = [], string $field = "", string $title = "") {
        if (!class_exists($value)) {
            return $this->packageError($field, $title, "配置错误");
        }
        $instance = app($value);
        if (!($instance instanceof MemberLoginControllerInterface)) {
            return $this->packageError($field, $title, '配置需实现接口"\yunj\app\admin\controller\member\MemberLoginControllerInterface"');
        }
        return true;
    }

}