<?php

namespace yunj\core\validate;

use yunj\core\constants\BuilderConst;
use yunj\core\Validate;

class YunjImportValidate extends Validate {

    protected $rule = [
        BuilderConst::ID_KEY => 'require',
        BuilderConst::ASYNC_TYPE_KEY => 'require|in:import',
        'items' => 'require|json',
    ];

    protected $message = [
        BuilderConst::ID_KEY . '.require' => '访问错误',
        BuilderConst::ASYNC_TYPE_KEY.'.require' => '['.BuilderConst::ASYNC_TYPE_KEY.']参数缺失',
        BuilderConst::ASYNC_TYPE_KEY.'.in' => '['.BuilderConst::ASYNC_TYPE_KEY.']参数错误',
        'items.require' => '[items]参数缺失',
        'items.json' => '[items]参数错误',
    ];

    protected $scene = [
        'AsyncRequest' => [BuilderConst::ID_KEY, BuilderConst::ASYNC_TYPE_KEY, 'items'],
    ];

    protected function handleData(array &$data, $scene): void {
        if ($scene == 'AsyncRequest') {
            switch ($data[BuilderConst::ASYNC_TYPE_KEY]) {
                case "import":
                    $this->handleDataByImport($data);
                    break;
            }
        }
    }

    private function handleDataByImport(&$data) {
        $data['items'] = json_decode($data['items'], true);
        $data["count"] = count($data['items']);
        return $data;
    }

}