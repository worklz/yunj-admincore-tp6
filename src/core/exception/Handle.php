<?php

namespace yunj\core\exception;

use think\db\exception\DataNotFoundException;
use think\db\exception\ModelNotFoundException;
use think\exception\Handle as ThinkHandle;
use think\exception\HttpException;
use think\exception\HttpResponseException;
use think\exception\ValidateException;
use think\Response;
use Throwable;
use yunj\core\exception\ResponseJsonException;

/**
 * 应用异常处理类
 */
class Handle extends ThinkHandle {
    /**
     * 不需要记录信息（日志）的异常类列表
     * @var array
     */
    protected $ignoreReport = [
        HttpException::class,
        HttpResponseException::class,
        ModelNotFoundException::class,
        DataNotFoundException::class,
        ValidateException::class,
    ];

    /**
     * 记录异常信息（包括日志或者其它方式记录）
     *
     * @access public
     * @param  Throwable $exception
     * @return void
     */
    public function report(Throwable $exception): void {
        // 使用内置的方式记录异常日志
        //parent::report($exception);

        if (!($exception instanceof ResponseJsonException)) log_exception($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @access public
     * @param \think\Request $request
     * @param Throwable $e
     * @return Response
     */
    public function render($request, Throwable $e): Response {
        // 添加自定义异常处理机制

        // 其他错误交给系统处理
        // return parent::render($request, $e);

        // yunj tp6
        // tp6接收json类型请求时会抛出json_encode后的数据，由于系统中很多地方包含非utf-8格式字符，导致不能正常显示错误消息
        // 所以我们接管“异常处理”屏蔽掉系统json_encode错误异常的处理
        $this->isJson = false;
        if ($e instanceof HttpResponseException) {
            return $e->getResponse();
        } elseif ($e instanceof HttpException) {
            return $this->renderHttpException($e);
        } else {
            return $this->convertExceptionToResponse($e);
        }
    }
}
