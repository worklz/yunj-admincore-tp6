<?php

namespace yunj\core\exception;

use yunj\core\response\Json;
use think\exception\HttpResponseException;

class ResponseJsonException extends HttpResponseException {

    public function __construct(Json $response) {
        parent::__construct($response);
    }

    public function getResponse(): Json {
        return $this->response;
    }

}